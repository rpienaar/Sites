<!doctype html>
<html lang="us">
	<head>
    <meta charset="utf-8">

    <title></title>		 

    <link href="<?php echo base_url("res/images/favicon.ico"); ?>" rel="shortcut icon" />

	<link rel="stylesheet" href="<?php echo base_url("res/css/main.css"); ?>" type="text/css" />		
		
    <script src="<?php echo base_url("res/js/jquery-1.8.2.min.js"); ?>" type="text/javascript" ></script>
	<script src="<?php echo base_url("res/js/main.js"); ?>" type="text/javascript" ></script>
	
  </head>
  <body>

