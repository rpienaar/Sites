// GENERIC JS CODE  -------------------------------------------------------------------------------------------------------------------

var mod,rest_url,set,next_command,onrequest_cmd;

function refresh(){
	location.href = 'app.php';
}

function maximize(){
	$('main_menu_tbl').set('width','100%');
}

function minimize(){
	$('main_menu_tbl').set('width','70%');
}

function ajax_get_call(mod,rest_url,set,next_command,onrequest_cmd){
	var c = new Request({
		url:mod+rest_url,
		method:'get',
		onRequest:function(){
			if(onrequest_cmd != undefined){
				eval(onrequest_cmd);
			}
		},
		onComplete:function(response){
			//alert('GET : '+response);
			if(set != undefined){
				$(set).set('html',response);
			}
			//alert(response);
			if(next_command != undefined){
				//alert(next_command);
				eval(next_command);
			}
		}
	}).send();
}

function ajax_post_call(mod,frm_id,set){
	var a = new Request({
		url:mod,
		method:'post',
		data: $(frm_id).toQueryString(),
		onComplete:function(response){
			//alert('POST : '+response);
			if(set != undefined){
				$(set).set('html',response);
			}
		}
	}).send();
}


document.onkeypress = function(){
		if( window.event.keyCode == 13 ){
				return false;
		}else{
				return true;
		}
}

// ------------------------------------------------------------------------------------------------------------------------------------

var elemref;

// --- hide
function h_elem(elemref){
  $(elemref).display='none';
}

function h_elem_html(elemref, elemrefhtml,html){
  h_elem(elemref);
  $(elemrefhtml).set('html',html);
}

function h_elem_html_onclick(elemref, elemrefhtml,html, elemrefaction,action){
  h_elem_html(elemref, elemrefhtml,html);
  $(elemrefaction).set('onclick',action);
}

// --- show
function s_elem(elemref){
  $(elemref).display='';
}

function s_elem_html(elemref, elemrefhtml,html){
  s_elem(elemref);
  $(elemrefhtml).set('html',html);
}

function s_elem_html_onclick(elemref, elemrefhtml,html, elemrefaction,action){
  s_elem_html(elemref, elemrefhtml,html);
  $(elemrefaction).set('onclick',action);
}

// ------------------------------------------------------------------------------------------------------------------------------------

function timed_sleep(naptime){
  var now = new Date();
  var alarm;
  var startingMSeconds = now.getTime();
  do {
    alarm = new Date();
    alarmMSeconds = alarm.getTime();
  } while(alarmMSeconds - startingMSeconds < naptime) ; 
}
