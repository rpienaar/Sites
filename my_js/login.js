
var c_name,value,expiredays;

function open_cookie_jar(){
  try{
	var c =getCookie('user');
	if(c != ''){
		var split = c.indexOf(">");
		var u = c.substring(0,split);
		var p = c.substring(split + 1);
		$('log_username_i').value = u;
		$('log_password_i').value = p;
		$('remember').checked = true;
	}
	return true;
  }catch(e){
    return false;
  }
}

function getCookie(c_name){
  try{
	if (document.cookie.length>0){
	 c_start=document.cookie.indexOf(c_name + "=");
	 if (c_start!=-1){ 
    c_start=c_start + c_name.length+1; 
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
   } 
  }
return "";
  }catch(e){
    return false;
  }
}

function login(){
  try{
		var username = $('log_username_i').value;
		var password = $('log_password_i').value;
		if($('remember').checked == true){
			bake_cookie('user',username,password,31); // create cookie
		}else{
			if($('remember').checked == false){
				bake_cookie('user',username,password,-55); //delete cookie
			}
		}
		document.forms["login_frm"].submit();
		return true;
  }catch(e){
    return false;
  }
}

function bake_cookie(u,uv,pv,expiredays){
	try{
		var exdate=new Date();
		exdate.setDate(exdate.getDate()+expiredays);
		document.cookie=u+"="+escape(uv)+">"+escape(pv)+
		((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
		return 'true';
	}catch(e){
		return 'false';
	}
}

document.onkeypress = function(){
	if( window.event.keyCode == 13 ){
		document.forms["login_frm"].submit();
	}
}




