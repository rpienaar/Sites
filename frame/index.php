<?php
require('classes/render.php');
$r = new render();

if(!empty($error)){
  $error_html = $r->ce("tr","",
                  $r->ce("td","",$error)
                );
}else{
  $error_html = "";
}
$out = $r->ce_doctype().
       $r->ce("html","xmlns='http://www.w3.org/1999/xhtml'",
         $r->ce("head","",
           $r->cse("meta","http-equiv='Content-Type'|Content='text/html; charset=utf-8'").
           $r->ce("title","","frame").
           $r->ce("script","type='text/javascript'|src='../js/mootools-1.2.4-core-nc.js'","")
         ).
         $r->ce("body","","")
       );
$r->display_html($out);
?>