<?php

define('DBUSER','root');
define('DBPASS','');
define('DBNAME','webproject');
define('DBHOST','localhost');

require('../../web/classes/class_checker.php');
require('../../web/classes/cdate.php');
require('../../web/classes/ez_sql_core.php');
require('../../web/classes/ez_sql_mysql.php');
require('../../web/classes/cdbcon.php');
require('../../web/classes/common.php');
require('../../web/classes/render.php');
require('../../web/classes/dbif.php');

$dbif = new dbif();

$r = new render();

$year_int = cdate::year_int();
$month_int = cdate::month_int();
$day_int = cdate::day_int();

$month_days = cdate::month_nmr_of_days_list($year_int);
$months = cdate::month_list();

// expand current month



// THIS PAGE WILL BE THE VIEW PAGE 
?>

<html>
  <head>
   <script type="text/javascript"  src="../js/app.js"></script>
   <script type="text/javascript"  src="../../web/js/mootools-1.2.4-core-nc.js"></script>
  </head>
  <body>
<?php

function create_month_table($month_int,$month_days,$r,$year_int,$dbif){
  $current_month_int = cdate::month_int();
  $out = "";
  $tds = "";
  $tr_closed = false;
  $days = $month_days[$month_int-1];
  $column_max = 6;
  $column_count = 0;
  for( $d = 1; $d < $days+1 ; $d++ ){
    if($current_month_int == $month_int){
      $sql = "SELECT * FROM `calendar_activities` WHERE DATE(`Timestamp`) = '2012-".cdate::pad_date_value($month_int)."-".cdate::pad_date_value($d)."'";
      $acts = $dbif->get_results($sql);      
      $day_acts = '';
      $act_count = 0;
      $act_limit = 2;
      if(is_array($acts)){
        foreach( $acts as $single_day_act ){
          if($act_count==$act_limit){
            $day_acts .= $r->ce("p",'',$r->ce("a",'href="#"|onclick="expand_"',"View All"));
            break;
          }
          $day_acts .= $r->ce("p",'',$single_day_act->Activity);
          $act_count++;
        }
      }
      $day_html = cdate::day_name($year_int,$month_int,$d).$r->ce("p",'',$d).$day_acts;
    }else{
      $day_html = cdate::day_name($year_int,$month_int,$d).$r->ce("p",'',$d);
    }
    
    $tds .= $r->ce("td",' ',$r->ce("p",'',$day_html));
    if($column_count == $column_max){
      $out .= $r->ce("tr",'',$tds);
      $tds = '';
      $column_count=0;
      $tr_closed = true;
    }else{
      $tr_closed = false;
      $column_count++;
    }
  }
  if($tr_closed == false){
    $out .= $r->ce("tr",'',$tds);
  }
  return $r->ce("table",'cellpadding="10"| cellspacing="10"|  border="1"| style="border-collapse:collapse;"',$out);
}

$c = 1;
$out = '';
foreach($months as $month){
  $tds = $r->ce("td",'valign="middle"',$month).$r->ce("td",'id="month_'.$c.'"| style="display:none;" ',create_month_table($c,$month_days,$r,$year_int,$dbif));
  $out .= $r->ce("tr",' ',$tds);
  $c++;
}
$out = $r->ce("table",'',$out);
$r->display_html($out);

?>

<script type="text/javascript" >
  expand_month(<?php print $month_int; ?>);
</script>

  </body>
</html>