<?php

check_dependancy('cfile.php');

class photodiary{
  static $dbcon;
  
  function photodiary(){
    //$dbclass = new cdbcon();
  	//$db = $dbclass->return_db_con();
		//photodiary::$dbcon = $db;
  }

  function listall(){
    //$users = photodiary::$dbcon->get_results("SELECT * FROM `user` ");
    //return $users;
  }

	function create_photo($id){
		if($id != 0){
			dbif::query("INSERT INTO `photo` (`FileId`,`UserId`) VALUES ('".$id."','".$_SESSION['ui']."') ");
		}
	}

	function my_photos(){
		//$query = " SELECT * FROM `photo` JOIN `file` ON ( `photo`.`FileId` = `file`.`FileId` ) WHERE `photo`.`UserId` = ".$_SESSION['ui']." order by UploadDate DESC";
		//return $this->loop_photos($query);
	}
	
	function all_photos_today(){
		//$query=" SELECT * FROM `photo` JOIN `file` ON ( `photo`.`FileId` = `file`.`FileId` ) WHERE UploadDate = Date(now()) AND `photo`.`UserId` = ".$_SESSION['ui']." order by UploadDate DESC";
		$query=" SELECT * FROM `photo` JOIN `file` ON ( `photo`.`FileId` = `file`.`FileId` ) WHERE UploadDate = Date(now()) order by UploadDate DESC";
		$photos = dbif::get_results($query);
		$out='';
		if(is_array($photos)){
			foreach($photos as $photo){
				$imglink = render::cse("img",'src="'.constant('UPLOADS_THUMBS').cfile::get_thumbnail_image_name($photo->Name).'"');
				$out .= render::ce("div",'align="center"',$imglink);
			}
		}
		return $out;
	}
	
	function photo_timeline(){
		$out = '';
		$year_days_array = cdate::month_nmr_of_days_list( cdate::year_int() );
		$month_name_array = cdate::month_list();
		$months_max = 12;
		$out .= "<table border=\"1\" style=\"corder-collapse: collapse;\">";
		$out .= "<tr> <td colspan=\"2\"> <table align=\"center\" cellspacing=\"0\" cellpadding=\"0\"><tr> <td><a href=\"#\">Expand All</a></td> <td><a href=\"#\">Collapse All</a></td> </tr></table> </td> </tr>";
		for( $month = 0; $month < $months_max; $month++ ){ // Months
			$img = "<img  src=\"../img/plus.png\" style=\"padding-right:20px;\" id=\"month_".$month."\" onclick=\"expand(this.id,'month_tbl_".$month."');\" />";
			$out .= "<tr><td valign=\"middle\" >".$img.$month_name_array[$month]."<hr/><table style=\"display:none;\" id=\"month_tbl_".$month."\" >";
			$out .= "<tr>";
			$out .= $this->loop_days($year_days_array,$month_name_array,$month);
			$out .= "</tr>";
			$out .= "</table></td></tr>";
		}
		$out .= "</table>";
		return $out;
	}
  
	function loop_days($year_days_array,$month_name_array,$month){
		$column_count = 0;
		$column_max = 4;	
		// LIMITED TO A YEAR, please complete this 
		$year_int = cdate::year_int();
		$out = '';
		for( $day = 1; $day < $year_days_array[$month]+1; $day++ ){ // Days
			if( $column_count == $column_max ){
				$column_count=0;
				$out .= "</tr><tr>";
			}
			$query = "SELECT * FROM `file` JOIN `photo` ON (`file`.`FileId` = `photo`.`FileId`) WHERE UploadDate = '".$year_int."-".($month+1)."-".$day."' ";
			$photo = dbif::get_row($query);
			if(is_object(@$photo)){
				$img = constant('UPLOADS_THUMBS').cfile::get_thumbnail_image_name($photo->Name); // IMPLEMENT ALBUM COVER here, so that only the cover photo is used for the day
			}else{
				$img = "../img/default-thumbnail-small.jpg";
			}
			$DateDayName = cdate::day_name($year_int,$month+1,$day);
			$out .= "<td class=\"thumb\"><p>".$year_int." ".$month_name_array[$month]." <span style=\"font-weight:bold;\">".$day."</span> ".$DateDayName."</p><img src=\"".$img."\" /><hr /></td>";
			$column_count++;
		}
		$column_count=0;	
		return $out;
	}
}
?>