<?php
session_start();

ini_set('session.gc_maxlifetime',8*60*60);
putenv("TZ=Africa/Johannesburg");
putenv("date.timezone=Africa/Johannesburg");

require('../../web/classes/class_checker.php');
require('../../web/classes/render.php');
require('../conf/constants.php');
require('../conf/paths.php');
require('../conf/dbsettings.php');
require('../../web/classes/db_system.php');
require('../../web/classes/common.php');
require('../../web/classes/dbif.php');
require('../../web/classes/dbhtml.php');
//require('theme.php');
//require('status.php');
require('../../web/classes/cdate.php');
require('../../web/classes/schema.php');
require('../../web/classes/button.php');
require('../../web/classes/pages.php');
require('../../web/classes/userdetails.php');
require('../../web/classes/log.php');
//require('ccalendar.php');
require("../../web/classes/cscope.php");
require('../../web/classes/group.php');
require('../../web/classes/cfile.php');

// Photo Diary
require('../classes/photodiary.php');
require('../classes/photoalbum.php');


$current_page = common::get_currecnt_page();
//$location = constant('ACTIVE_PATH');

$cdate       = new cdate();
$s           = new schema();
$b           = new button();
$p           = new pages();
$userdetails = new userdetails();
$r           = new render();
$g           = new group();

$dbif        = new dbif();

//$schema_count = $s->get_schema_count();

require("../../web/classes/check_session.php");

?>