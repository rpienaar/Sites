<?php

//Folder Paths
define('MODULES_FOLDER',   "modules/");
define('MODULES',          "../".constant('MODULES_FOLDER'));

define('CLASSES',          "../classes/");
define('FRAMEWORK_CLASSES',"../../web/classes/");

define('CSS',              "../css/");
define('THEME_CSS',        "../theme_css/");
define('IMG',              "../img/");
define('INCLUDE_FOLDER',   "inc/");
define('INC',              "../".constant('INCLUDE_FOLDER'));
define('INSTALL',          "../install/");

define('JS',               "../js/");
define('FRAMEWORK_JS',     "../../web/js/");

define('PAGES_FOLDER',     "pages/");
define('PAGES',            "../".constant('PAGES_FOLDER'));
define('VIDEOS',           "../video/");
define('VIDEO_PLAYER',     "../video_player/");
define('MUSIC',            "../music/");
// define('MUSIC_PLAYER',  "../music_player/xspf_player.swf");
define('MUSIC_PLAYER',     "../music_player/xspf_jukebox/xspf_jukebox.swf");
define('PLAYLISTS_FOLDER', "playlists/");
define('PLAYLISTS',        "../".constant('PLAYLISTS_FOLDER'));
define('UPLOADS',          "../uploads/");
define('UPLOADS_THUMBS',   "../uploads/thumbs/");
define('SCRIPT',           "escript/");
define('DBSETTINGS',       "../conf/dbsettings.php");
define('PATHSETTINGS',     "../conf/paths.php");
define('SRC_FOLDER',       "scripts/");
define('SRC',              "../".constant('SRC_FOLDER'));
define('AVATAR',           'avatars/');


define('UPLOAD_FOLDER','uploads');

define('MOOTOOLS',"mootools-1.2.4-core-nc.js");

// Statusses
define('ENABLED', 1);
define('DISABLED',2);
define('DELETED', 3);

//Actions
define('ADD',   1);
define('EDIT',  2);
define('DELETE',3);

// build a URL switcher
define('ONLINE_URL', "../");
define('OFFLINE_URL',"../");
define("ACTIVE_URL", constant('ONLINE_URL'));

// build a scope page
define('SCOPE_PUBLIC', 1);
define('SCOPE_PRIVATE',2);
define('SCOPE_SYSTEM', 3);
define('SCOPE_FEED',   4);
define('SCOPE_FEED_CYANIDE',   5);

// build a mode changer area
define('DEBUG',      1);
define('SAFE_MODE',  2);
define("ACTIVE_MODE",constant('DEBUG'));

define('AV',"avatar");

define('MYSQL',1);
define('PSQL',2);

define('DB_SYSTEM',constant('MYSQL'));

define('LOGGED_IN_PAGE','../pages/home.php');
define('FRAMEWORK_INC','../../web/inc/');

?>
