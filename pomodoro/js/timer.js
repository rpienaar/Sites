
function check_timer(){
  
}







function start_timer(){
  var desc = prompt("Pomodoro description:");
  $('reason').set('value','Pomodoro Completed.');
  $('seconds').set('value',0);
  run_timer();
  record_pomodoro(desc);
  
  ajax_get_call(mod,rest_url,set,next_command,onrequest_cmd);
  
}

function run_timer(){
  max_seconds = 1500;
  count = ($('seconds').value-0);
  if(count != max_seconds){
    minutes = seconds_to_minutes_str(max_seconds-count);
    $('timer_td').set('html',minutes);
    $('seconds').set('value',count+1);
    setTimeout('run_timer()',999);
  }else{
    $('reason_td').set('html',$('reason').value);
    // record completed
  }
}

function seconds_to_minutes_str(totalSeconds){
  hours = totalSeconds / 3600;
  totalSeconds %= 3600;
  minutes = totalSeconds / 60;
  seconds = totalSeconds % 60;
  
  str_sec = seconds+"";
  if( str_sec.length == 1 ){
    str_sec = "0"+str_sec;
  }else{
    str_sec = seconds;
  }
  
  return Math.floor(minutes)+":"+str_sec;
}

function reset(){
  $('seconds').set('value',1500);
  $('timer_td').set('html',"00:00");
  $('reason').set('value','Pomodoro Reset.');
  // record cancel
}

function interupt(){
  // record interuption
}