var mod,rest_url,set,next_command,onrequest_cmd;
function ajax_get_call(mod,rest_url,set,next_command,onrequest_cmd){
	var c = new Request({
		url:mod+rest_url,
		method:'get',
		onRequest:function(){
			if(onrequest_cmd != undefined){
				eval(onrequest_cmd);
			}
		},
		onComplete:function(response){
			if(set != undefined){
				$(set).set('html',response);
			}
			if(next_command != undefined){
				eval(next_command);
			}
		}
	}).send();
}
function ajax_post_call(mod,frm_id,set){
	var a = new Request({
		url:mod,
		method:'post',
		data: $(frm_id).toQueryString(),
		onComplete:function(response){
			if(set != undefined){
				$(set).set('html',response);
			}
		}
	}).send();
}