<?php

function ingredient_html($ingridient_array){
	$ret = '';
	$ret .= "<table border=\"1\">";
	$ret .= "<tr><td>Type</td><td>Description</td></tr>";
	foreach($ingridient_array as $ing_item){
		$ret .= "<tr><td>".$ing_item['type']."</td><td>".$ing_item['description']."</td></tr>";
	}
	$ret .= "</table>";
	return $ret;
}

?>
<html>
	<head>
		<title>
			Grocery Manager
		</title>
	</head>
	<body>
	
		<p>Virtual inventory</p>
		<table border="1">
			<tr>
				<td>Food Type</td>
				<td>Description</td>
			</tr>
			<?php 
				$items = array(
					array('type'=>'carbohydrates','description'=>'rice','quantity'=>'200','quantity_label'=>'g'),
					array('type'=>'carbohydrates','description'=>'sugar','quantity'=>'1','quantity_label'=>'kg')
				);
				foreach($items as $item){
					print "<tr>";
					print "<td>".$item['type']."</td>";
					print "<td>".$item['description']."</td>";
					print "</tr>";
				}
			?>
			</table>
			
			<br/>
			
			<p>Recipies</p>
			<table border="1">
				<tr>
					<td>name</td>
					<td>description</td>
					<td>ingredients</td>
				</tr>
				<?php
					$recipies = array(
						array('name'=>'Chicken ala King',
						      'description'=>'Chicken a la king is made with chicken, butter, mushrooms and a few other ingredients. This easy recipe will allow you to rustle up a scrumptious dinner – which can be served with rice and perhaps a fresh side salad – in no time at all.',
						      'ingredients'=>array(
							      array('type'=>'fats',           'description'=>'oil','instruction_desc'=>'','quantity'=>'30','quantity_label'=>'ml'),
							      array('type'=>'carbohydrate',   'description'=>'onion','instruction_desc'=>'chopped','quantity'=>'1','quantity_label'=>''),
										array('type'=>'carbohydrate',   'description'=>'mushroom','instruction_desc'=>'Button mushrooms, sliced','quantity'=>'80','quantity_label'=>'g'),
										array('type'=>'carbohydrate',   'description'=>'Flour','instruction_desc'=>'Flour','quantity'=>'30','quantity_label'=>'ml'),
						      	array('type'=>'multi_nutrition','description'=>'milk','instruction_desc'=>'milk','quantity'=>'375','quantity_label'=>'ml','multi_nutrition_types'=>array('fat','protein','vitamins_minerals')),
						      	array('type'=>'misc',           'description'=>'Chicken Stock','instruction_desc'=>'Chicken Stock','quantity'=>'185','quantity_label'=>'ml'),
						      	array('type'=>'Proteins',       'description'=>'Chicken','instruction_desc'=>'cooked and chopped','quantity'=>'500','quantity_label'=>'g'),
										array('type'=>'fats',           'description'=>'cream','instruction_desc'=>'savoury cream, salt and pepper to taste','quantity'=>'45','quantity_label'=>'ml'),
						      	array('type'=>'carbohydrate',   'description'=>'rice','instruction_desc'=>'Uncooked rice','quantity'=>'375','quantity_label'=>'ml'),
						      	array('type'=>'carbohydrate',   'description'=>'red pepper','instruction_desc'=>'chopped red pepper','quantity'=>'1','quantity_label'=>''),
						      	array('type'=>'carbohydrate',   'description'=>'parsley','instruction_desc'=>'Chopped parsley','quantity'=>'15','quantity_label'=>'ml')
									),
									'preparation_time_min'=>30,
									'serves'=>4
						)
					);
					
					foreach($recipies as $recipy){
						print "<tr>";
						print "<td>".$recipy['name']."</td>";
						print "<td>".$recipy['description']."</td>";
						print "<td>".ingredient_html($recipy['ingredients'])."</td>";
						print "</tr>";
					}
					
				?>
			</table>
			
		</table>
	</body>
</html>