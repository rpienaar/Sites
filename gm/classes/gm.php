<?php

class gm {
	static $dbcon;

	function gm(){
		$dbclass = new cdbcon();
  	gm::$dbcon = $dbclass->return_db_con();
	}

	function listall_inventory_html(){
		$results = gm::$dbcon->get_results("SELECT
		                                   `gm_inventory_type`.`Description` as type_desc,
		                                   `gm_inventory`.`Description`,
		                                   `gm_inventory`.`Quantity`,
		                                   `gm_quantity`.`label` as QuantityLabel
		                                    FROM `gm_inventory` 
		                                    JOIN `gm_inventory_type` ON (`gm_inventory`.`TypeId` = `gm_inventory_type`.`Id`)
		                                    JOIN `gm_quantity` ON (`gm_inventory`.`QuantityLabelId` = `gm_quantity`.`Id`)
		                                    ");
		return $this->foreach_inventory_html($results);
	}
	
	function foreach_inventory_html($inventory){
		$ret = '<table class="page_tbl" border="1">';
		$ret .= "<tr> <td>Type</td><td>Description</td><td>Quantity</td><td>Quantity Label</td> </tr>";
		if(is_array($inventory)){
			foreach($inventory as $inv_item){
				$ret .= "<tr>";
				$ret .= "<td>".$inv_item->type_desc."</td><td>".$inv_item->Description."</td><td>".$inv_item->Quantity."</td><td>".$inv_item->QuantityLabel."</td>" ;
				$ret .= "</tr>";
			}
		}
		$ret .= "</table>";
		return $ret;
	}
	
	function listall_recipy_html(){
		
	}
	
	function inventory_type_select_html(){
		return $this->sql_select_html("gm_inventory_type","Id","Description");
	}
	
	function quantity_label_select_html(){
		return $this->sql_select_html("gm_quantity","Id","label");
	}
	
	function sql_select_html($table,$value_sql_column,$description_sql_column){
		$sel = '<select>';
		$types = gm::$dbcon->get_results("SELECT * FROM `".$table."` ");
		if(is_array($types)){
			foreach($types as $type){
				$sel .= '<option value="'.$type->$value_sql_column.'">'.$type->$description_sql_column.'</option>';
			}
		}
		$sel .= '</select>';
		return $sel;
	}
	
}

?>