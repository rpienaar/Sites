<?php

require('conf/dbsettings.php');
require('conf/constants.php');

require('../classes/class_checker.php');
require('../classes/ez_sql_core.php');
require('../classes/ez_sql_mysql.php');
require('../classes/cdbcon.php');
require('../classes/common.php');
require('classes/gm.php');

?>
<html>
	<head>
		<title>Grocery Manager</title>
		<link rel="stylesheet" href="css/main.css" />
		<script type="text/javascript" src="../js/mootools-1.2.4-core-nc.js"></script>
		<script type="text/javascript" src="js/s.js"></script>
	</head>
	<body>
	<table class="page_tbl">
		<tr>
			<td><a href="index.php">Home</a></td><td><a href="inventory.php">Inventory</a></td><td><a href="recipies.php">Recipies</a></td>
		</tr>
	</table>