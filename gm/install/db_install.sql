INSERT INTO `gm_quantity` (`label`) VALUES ('ml');
INSERT INTO `gm_quantity` (`label`) VALUES ('l');
INSERT INTO `gm_quantity` (`label`) VALUES ('mg');
INSERT INTO `gm_quantity` (`label`) VALUES ('g');
INSERT INTO `gm_quantity` (`label`) VALUES ('amount');

INSERT INTO `gm_inventory_type` (`Description`) VALUES ('carbohydrate');
INSERT INTO `gm_inventory_type` (`Description`) VALUES ('protein');
INSERT INTO `gm_inventory_type` (`Description`) VALUES ('fat');
INSERT INTO `gm_inventory_type` (`Description`) VALUES ('vitamins_minerals');
INSERT INTO `gm_inventory_type` (`Description`) VALUES ('multiple');

INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (1,'oil','30',1);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (1,'onion','1',5);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (1,'mushroom','80',4);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (1,'flour','80',1);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (5,'milk','375',1);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (5,'chicken stock','185',1);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (2,'chicken','500',4);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (5,'savoury cream','45',1);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (1,'rice','375',1);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (2,'red pepper','1',5);
INSERT INTO `gm_inventory` (`TypeId`,`Description`,`Quantity`,`QuantityLabelId`) VALUES (2,'parsley','15',1);

/*
array('type'=>'fats',           'description'=>'oil','instruction_desc'=>'','quantity'=>'30','quantity_label'=>'ml'),
array('type'=>'carbohydrate',   'description'=>'onion','instruction_desc'=>'chopped','quantity'=>'1','quantity_label'=>''),
array('type'=>'carbohydrate',   'description'=>'mushroom','instruction_desc'=>'Button mushrooms, sliced','quantity'=>'80','quantity_label'=>'g'),
array('type'=>'carbohydrate',   'description'=>'Flour','instruction_desc'=>'Flour','quantity'=>'30','quantity_label'=>'ml'),
array('type'=>'multi_nutrition','description'=>'milk','instruction_desc'=>'milk','quantity'=>'375','quantity_label'=>'ml'),
array('type'=>'misc',           'description'=>'Chicken Stock','instruction_desc'=>'Chicken Stock','quantity'=>'185','quantity_label'=>'ml'),
array('type'=>'Proteins',       'description'=>'Chicken','instruction_desc'=>'cooked and chopped','quantity'=>'500','quantity_label'=>'g'),
array('type'=>'fats',           'description'=>'cream','instruction_desc'=>'savoury cream, salt and pepper to taste','quantity'=>'45','quantity_label'=>'ml'),
array('type'=>'carbohydrate',   'description'=>'rice','instruction_desc'=>'Uncooked rice','quantity'=>'375','quantity_label'=>'ml'),
array('type'=>'carbohydrate',   'description'=>'red pepper','instruction_desc'=>'chopped red pepper','quantity'=>'1','quantity_label'=>''),
array('type'=>'carbohydrate',   'description'=>'parsley','instruction_desc'=>'Chopped parsley','quantity'=>'15','quantity_label'=>'ml')
*/