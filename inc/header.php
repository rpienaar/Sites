<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<meta http-equiv="Pragma" content="no cache" />
	<title>Welcome <?php print $_SESSION['user']; ?></title>
	
	<script type="text/javascript" src="<?php print constant('FRAMEWORK_JS').constant('MOOTOOLS');?>"></script> 
	
	<script type="text/javascript" src="<?php print constant('FRAMEWORK_JS');?>g.js"></script>
	<script type="text/javascript" src="<?php print constant('FRAMEWORK_JS');?>page.js"></script>
	<script type="text/javascript" src="<?php print constant('FRAMEWORK_JS');?>menu.js"></script>
	
	
	<style type="text/css">
	<?php if($trans_buttons == "1"){ ?>
	  .btn{      opacity:0.65;filter:alpha(opacity=65);}
	  .btn:HOVER{opacity:1;   filter:alpha(opacity=100);}
	<?php }else{ ?>
	  .btn{      opacity:1;filter:alpha(opacity=100) !important ;}
	  .btn:HOVER{opacity:1;filter:alpha(opacity=100) !important ;}
	<?php } ?>
	td#page_td{
		background-color:white;
		opacity:<?php print($opacity*0.01);?>;
		filter:alpha(opacity=<?php print $opacity;?>);
	}
	td#full_page{
		background-color:white;
		opacity:<?php print($opacity*0.01);?>;
		filter:alpha(opacity=<?php print $opacity;?>);
	}
	</style>
	
	<link rel="stylesheet" href="<?php print constant('THEME_CSS');?><?php print $theme;?>" />
	<link rel="stylesheet" href="<?php print constant('THEME_CSS');?>main_style.css" />
	<link rel="shortcut icon" href="<?php print constant('IMG');?>favicon.ico" />

</head>

<?php 

if($fname==NULL || $fname=="NULL"){?><body id="body" ><?php }
else{?><body id="body" background="<?php print constant('UPLOADS');?><?php print $fname;?>"><?php } ?>

<div align="center" id="dim" class=”dimmer” style=" background-color: rgb(0, 0, 0);opacity: 0.7;-moz-opacity:0.70;filter: alpha(opacity=70);z-index: 20;height: 100%;width: 100%;background-repeat:repeat;position:fixed;top: 0px;left: 0px; " >
	<div style="position:absolute; display: none;" id="load_img" ><img src="../img/ajax-loader.gif" /></div>
</div>

<br />
<table id="main_menu_tbl" width="70%" align="center" class="page_table">
  <tr>
    <td>
      <table height="10%" id="menu" style="display:none; position:absolute; left:2px; right:2px; top:2px; bottom:2px; z-index:2;" cellpadding="2" cellspacing="2">
      	<tr> 
      		<td valign="top" style="width:50px;"> <?php require(constant("FRAMEWORK_INC")."logout.php");?> </td> 
	      	<td valign="top"> <button class="btn" onclick="load_page('home.php');" >Home</button><br /> </td>
      		<?php
      		$allowed_buttons = $b->get_user_buttons($_SESSION['user_type_id']);
      		if(is_array($allowed_buttons)){ foreach ($allowed_buttons as $button){ print $button->button_text; } }
      		?>
      	</tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td id="page_td" style="z-index:1">
      <table width="100%" >
      	<tr>
	      	<td align="left" width="250px">
	      		<a href="#" >
	      			<img id="user_avatar_img" onclick="load_page('my_settings.php');" src="../img/small_loading.gif" >
	      		</a>
	      	</td>
      		<td valign="top">
      			<div align="center">
      				<img id="page_picture" src="../img/small_loading.gif" />
      				<br />
      				<img class="click_img" onclick="lcp();" src="../img/refresh.jpg" />
      			</div>
      		</td>
      		<td valign="top" width="250px" ><font size="3"><b>User:</b><?php print $_SESSION['user']; ?></font></td>
      	</tr>
      </table>
  </tr>
  <tr>
  	<td align="center" id="full_page">
  	