<?php require("classes/inc.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <title>RDBMS</title>
  <script type="text/javascript" src="js/moo.js"></script>
	<script type="text/javascript" src="js/s.js"></script>
	<link rel="stylesheet" href="css/main.css" />
</head>
<body>
<form name="db_connect" action="p.php" method="post">
  <table id="rdbms_table" >
    <tr>
    <?php
    if(@$_SESSION['connected'] == true and !empty($_SESSION['host']) and !empty($_SESSION['user'])){
      ?>
      <td>
        <table style="border-collapse:collapse;position:absolute;left:0px;right:0px;top:0px;bottom:0px;" border="1" cellpadding="4" cellspacing="4">
          <tr>
            <td style="height:31px;">Connected <img id="loader" src="img/ajax-loader.gif" style="display:none;" /> </td><td><input type="hidden" name="disconnect" value="1" /> <button>Disconnect</button></td>
          </tr>
          <tr>
            <td valign="top">
              <table style="border-collapse:collapse;" border="1" cellpadding="4" cellspacing="4" id="sel_database"></table>
              <table style="border-collapse:collapse;" border="1" cellpadding="4" cellspacing="4" id="sel_table"></table>
            </td>
            <td valign="top">
              <table style="border-collapse:collapse;" border="1" cellpadding="4" cellspacing="4" id="record_table"></table>
            </td>
          </tr>
        </table>
        <input type="hidden" name="table_selected" id="table_selected" />
        <script type="text/javascript" >get_databases();x();</script>
      </td>
    <?php }else{ ?>
      <td>
        <table>
          <tr> <td>Host:</td><td> <input type="text" name="host" />                             </td> </tr>
          <tr> <td>User:</td><td> <input type="text" name="user" />                             </td> </tr>
          <tr> <td>Password:</td><td> <input type="password" name="pass" />                         </td> </tr>
          <tr> <td colspan="2"><button onclick="document.db_connect.submit();">Connect</button> </td> </tr>
        </table>
      </td>
    <?php } ?>
    </tr>
  </table>
</form>
<br />

</body>
</html>