// --- todo ---
// 1) make a checker for data length. if length being used exceeds 255 use ajax with POST.
// 2) make a check for primary key, when deleting and inserting

//-------------------------------------------------------------------------------------------------
var database_name, table_name, col_name, col_value, amount_columns, edit_button_id, action_status, parent_node, toggle_button, toggle_status, object;
//-------------------------------------------------------------------------------------------------
function dc(){$('loading_image').style.display="";$('sel1').disabled="false";$('sel2').disabled="false";$('sel_database').disabled="false";$('sel_table').disabled="false";}
function ec(){$('loading_image').style.display = "none" ;$('sel1').disabled = "";$('sel2').disabled = "";$('sel_database').disabled = "";$('sel_table').disabled = "";}
function disable_table_selection(){$('sel2').disabled="true";$('sel_table').disabled="true";}
function da(){$('new_db_name').disabled = "true";$("create_db_button").disabled = "true";$('new_table').disabled = "true";$('num_fields').disabled = "true";$("create_table").disabled = "true";}
function ea(){$('new_db_name').disabled = "";$("create_db_button").disabled = "";$('new_table').disabled = "";$('num_fields').disabled = "";$("create_table").disabled = "";}

function era(action_status){
	if(action_status){
		$('del_button').style.display = '' ;
		$('edit_button').style.display='';
	}else{
		var num = 0 ;
		var enabled = false ;
		var elem = '' ;
		var elem_str = '';
		do {
			num++ ;
			elem_str = 'row_action_'+num ;
			elem = $(elem_str) ;
			try{ if(elem.checked == true){ enabled = true ;} }catch(e){ break ; }
		} while (elem) ;
		if(enabled != true){ $('del_button').style.display = 'none' ; $('edit_button').style.display='none'; }
	}
}

function pop_db_name(){
	if($("sel_database").value == ""){
		$('wtf').set('html', "<font color='red'>1) Select a Database</font>") ;
	}else{
		$('wtf').set('html', "<font color='red'>on database : "+$("sel_database").value+"</font>") ;
	}
	if ($('wtf').innerHTML == "<font color='red'>Select database</font>"){ 
		$('new_table').disabled = "true" ;
		$('num_fields').disabled = "true" ;
		$('create_table').disabled = "true" ;
	}else{
		$('new_table').disabled = "" ;
		$('num_fields').disabled = "" ;
		$('create_table').disabled = "" ;
	}
}

function select_database(database_name, parent_node){
	var SelectDatabaseRequest = new Request({
		method: 'get',
		url: 'modules/get_tables.php?dbname='+database_name,
		onRequest: function(){
			dc();
			$('record_table').set('html', '') ;
		},
		onComplete: function(response){
			ec() ;
			pop_db_name() ;
			if($(parent_node).innerHTML.indexOf("<") > 0 && $(parent_node).innerHTML.indexOf(">") > 0) { //then there is html
				
			}else{
				$(parent_node).set('html', $(parent_node).innerHTML+response);
			}
		}
	}).send();
	$('record_length').style.display = 'none' ;
	$('database_selected').value = database_name ;
}

function get_databases() {
var SelectTableRequest = new Request({
			method: 'get',
			url: 'modules/get_databases.php' ,
			onRequest: function(){ dc() ; },
			onComplete: function(response){
				ec();
				$('sel_database').set('html' , response);
				//alert(response) ;
			}
		}).send();
}


function select_table(table_name){
	$('record_length').style.display = 'none' ;
	var SelectTableRequest = new Request({
		method: 'get',
		url: 'modules/get_records.php?tblname='+table_name ,
		onRequest: function(){
			dc();
		},
		onComplete: function(response){
			ec() ;
			$('record_table').set('html', response) ;
			if(response > ""){
				$('actions').style.display = '' ;
				$('l_time').style.display = '';
				$('l_time').set('html', $('m_l_time').value) ;
			}
		}
	}).send() ;
	get_num_records(table_name, $('database_selected').value) ;
	if($('record_length').style.display == 'none'){
		$('record_length').style.display = '' ;
		var ar = $('amount_records').value ;
		if(ar > 20){
			$('records_selected_from').value = 0 ;
			$('records_selected_to').value = 20 ;
		}else{
			$('records_selected_from').value = 0 ;
			$('records_selected_to').value = Number(ar) ;
		}
	}
}

function get_num_records(table_name, database_name){
	var query_str = 'SELECT COUNT( * ) FROM `'+table_name+'`' ;
	var getNumRows = new Request({
		method: 'get',
		url: 'modules/sql_db_action.php?query='+query_str+'&dbname='+database_name,
		onComplete: function(response){
			$('amount_records').value = response ;
		}
	}).send() ;
}


function make_tbl_field_string(txt_box){
	var text_boxes;
	if(!($('table_field_names').value.contains(txt_box))){
		text_boxes = $('table_field_names').value ;
		text_boxes = text_boxes+txt_box+"," ;
		$('table_field_names').value = text_boxes;
	}
}

//function start_creating_new_table(table_name,amount_columns){
//	dc(); ;
//	da() ;
//	var i=0 ;
//	var TableAreaString = "" ;
//		for(i=0; i < amount_columns; i++){
//			TableAreaString=TableAreaString+"<tr><td><table><tr><td>" ;
//			TableAreaString=TableAreaString+"<b>Name</b>:</td><td><input type='text' id='col_name["+i+" /></td>" ;
//			TableAreaString=TableAreaString+"<td><b>Type</b>:</td><td><select id='col_data_type["+i+">"+$('data_types').innerHTML+"</select></td>" ;
//			TableAreaString=TableAreaString+"<td><b>Length</b>:</td><td><input type='text' id='col_length["+i+" /></td>" ;
//			TableAreaString=TableAreaString+"<td><b>Default value</b>:</td><td><select id='col_default["+i+">"+$("default_values").innerHTML+"</select></td>" ;
//			TableAreaString=TableAreaString+"<td colspan='2'><input type='text' id='default_value["+i+" /></td></tr>" ;
//			TableAreaString=TableAreaString+"<tr><td><b>Collation</b>:</td><td><select id='collation["+i+">"+$('collation').innerHTML+"</select></td>" ;
//			TableAreaString=TableAreaString+"<td><b>Attributes</b>:</td><td><select id='attributes["+i+">"+$('attributes').innerHTML+"</select></td>" ;
//			TableAreaString=TableAreaString+"<td><b>null</b>:</td><td><select id='null_switch["+i+" ><option value='NOT NULL'>NOT NULL</option><option value='NULL'>NULL</option></select></td>" ;
//			TableAreaString=TableAreaString+"<td><b>Index</b>:</td><td><select id='index_value["+i+">"+$('index').innerHTML+"</select></td>" ;
//			TableAreaString=TableAreaString+"<td><b>Auto increment</b>:</td><td><select id='ai["+i+"><option value=''> </option><option value='AUTO_INCREMENT'>AUTO_INCREMENT </option></select></td>" ;
//			TableAreaString=TableAreaString+"<td><b>Comments</b>:<input type='text' id='comments["+i+" /></td></tr></table></td></tr>" ;
//		}
//	TableAreaString = "<table cellpadding='4' cellspacing='4' border='1' style='border-collapse:collapse;'>"+TableAreaString+"</table>" ;
//	TableAreaString = TableAreaString + "<br /><b>Storage_engines</b>: <select id='storage_engine'>"+$('storage_engines').innerHTML+"</select>" ;
//	TableAreaString = TableAreaString + "<b>Table comments</b>: <input type='text' id='tbl_comments'  /> " ;
//	TableAreaString = TableAreaString + "<b>Table collation</b>: <select id='tbl_collation'>"+$('collation').innerHTML+"</select>" ;
//	TableAreaString = TableAreaString + "<br /><br /><div align='center'><button type='button' onclick='create_new_table('"+table_name+"','"+amount_columns+"');'>Create</button>"
//	TableAreaString = TableAreaString + "<button type='button' onclick='finished_creating_table();'>Cancel</button></div>" ;
//	$("creation_area").innerHTML = TableAreaString ;
//	$("new_table_row").style.display='' ; 
//}

//function create_new_table(table_name,amount_columns){
//	var new_column_name = new Array() ;
//	var new_data_type = new Array() ;
//	var new_data_length = new Array();
//	var new_default_value_type = new Array();
//	var new_default_value = new Array();
//	var new_collation = new Array();
//	var new_collate = new Array() ;
//	var new_attributes = new Array();
//	var new_null = new Array();
//	var new_index = new Array();
//	var new_ai = new Array();
//	var new_comments = new Array();
//	var column_data = new Array() ;
//	var new_table_sql_string = '' ;
//	for(var t = 0 ; t < amount_columns ; t++){
//		new_column_name[t] = $("col_name["+t+"]").value ;
//		new_data_type[t] = $("col_data_type["+t+"]").value;
//		new_data_length[t] = $("col_length["+t+"]").value ;
//		new_default_value_type[t] = $("col_default["+t+"]").value ;
//		new_default_value[t] = $("default_value["+t+"]").value ;
//		new_collation[t] = $("collation["+t+"]").value ;
//		new_collate[t] = $("collation["+t+"]").parentNode.label ;
//		new_attributes[t] = $("attributes["+t+"]").value ;
//		new_null[t] = $("null_switch["+t+"]").value ;
//		new_index[t] = $("index_value["+t+"]").value ;
//		new_ai[t] = $("ai["+t+"]").value ;
//		new_comments[t] = $("comments["+t+"]").value ;
//		column_data[t] = "`"+new_column_name[t]+"` " ;
//		if (new_data_type[t] == "TEXT") {
//			column_data[t] = column_data[t] + new_data_type[t]+" " ;
//		}else{
//			column_data[t] = column_data[t] + new_data_type[t]+"("+new_data_length[t]+") " ;
//		}
//		if(new_attributes[t] > ''){ column_data[t] = column_data[t] + new_attributes[t] }
//		if(new_collation[t] > '' && new_collate){ column_data[t] = column_data[t]+" CHARACTER SET "+new_collation[t]+" COLLATE "+new_collate[t]; }
//		column_data[t] = column_data[t] + new_null[t]+" " ;
//		if(new_default_value_type[t] == 'USER_DEFINED' && new_default_value[t] > ''){ column_data[t] = column_data[t] +"DEFAULT '"+new_default_value[t]+"' " ; }
//		if(new_ai[t] == "AUTO_INCREMENT "){ column_data[t] = column_data[t] + new_ai[t] ; }
//		if(new_index[t] > ''){ column_data[t] = column_data[t] + new_index[t] ; }
//		if(new_comments[t] > ''){ column_data[t] = column_data[t] + " COMMENT '"+new_comments[t]+"'" ; }
//	}
//	new_table_sql_string = "CREATE TABLE `"+$('sel_database').value+"`.`"+table_name+"` (" ;
//	column_data.each(function(column_data){
//		new_table_sql_string = new_table_sql_string+column_data+"," ;
//	}) ;	
//	new_table_sql_string = new_table_sql_string.substr(0,new_table_sql_string.length - 1) ; //remove the last ,
//	new_table_sql_string = new_table_sql_string+") ENGINE = "+$('storage_engine').value+" " ;
//	var NewTableRequest = new Request({
//		method: 'get',
//		url: 'modules/sql_action.php?query='+new_table_sql_string,
//		onComplete: function(){
//			finished_creating_table() ;
//			$("new_table").value = '' ;
//			$("num_fields").value = '' ;
//		}
//	}).send() ;
//}

//function finished_creating_table() {
//	$("creation_area").innerHTML = '' ;
//	ec() ;
//	ea() ;
//	$("new_table_row").style.display='none' ; 
///	select_database($("sel_database").value) ;
//}

function insert_record(){ 
	var children = new Array() ;
	var i, td, columnNames,insert_string ;
	var values = '' ;
	var ar = $('add_row') ;
	children = ar.childNodes ;
	for(i = 0 ; i< (children.length - 4) ; i++){ //-2 because the last two td's are for the image, and a empty td
		//td = ar.childNodes[i].childNodes[1].value ;
		if(i == (children.length - 5)){  values = values+"'"+td+"'" ;
		}else{ values = values+"'"+td+"'," ; }
	}
	columnNames = $('columns').value ;
	insert_string = "INSERT INTO `"+$('sel_database').value+"`.`"+$('sel_table').value+"` ("+columnNames+") VALUES ("+values+")" ;
	//alert(insert_string) ;
//	var InsertRecordRequest = new Request({
//		method: 'get',
//		url: 'modules/sql_action.php?query='+insert_string ,
//		onRequest: function(){
//			dc(); ;
//		},
//		onComplete: function(response){
//			select_table($('sel_table').value) ;
//			ec() ;
//		}
//	}).send();
}


function delete_record(col_name,col_value){ // works
	var answer = confirm("Do you really want to delete this record ?") ;
	if(answer == true){
		var DeleteRecordRequest = new Request({
			method: 'get',
			url: 'modules/delete_record.php?columnN='+col_name+'&columnV='+col_value ,
			onRequest: function(){
				dc();
			},
			onComplete: function(response){
				ec() ;
				var tbl = $('sel_table').value ;
				select_table(tbl) ;
			}
		}).send();
	}
}

function edit_row(record_number, edit_button_id){
	var row = "rec_row_"+record_number; //record row
	var counter = 0 ;
	var results = new Array() ;
	number_of_columns = ($(row).childNodes.length - 2) ;  // -2 , //because the edit button and delete button does not count
	
	for(var g = 0 ; g < number_of_columns ; g++){
		results[g] = $(row).childNodes[g].innerHTML ;
	}
	
	results.each(function(result){
		//$(row).childNodes[counter].innerHTML = "<input type='text' value='"+result+"' id='new_val_"+counter+"' />"
		$(row).childNodes[counter].set('html', "<input type='text' value='"+result+"' id='new_val_"+counter+"' />") ;
		counter++ ;
	}) ;
	$(edit_button_id).style.display = 'none'  ;
	var save_button_text = "save_button_"+record_number ;
	$(save_button_text).style.display = '' ;
}

//var column_id = '' ;
//function check_if_column_changed(column_id){
//	if($('modified_columns').value.indexOf(column_id) > -1){
//		//not inside
//	}else{
//		//already inside
//		$('modified_columns').value = $('modified_columns').value + column_id+"," ;
//	}
//}

// save_value_to_hiddenbox to get result array

//function save_edited_row(rec_num){
//	var row = 'rec_row_'+rec_num ;
//	var values = "(" ;
//	var column_count = $(row).childNodes.length - 3 ;
//	for(var vl = 0 ; vl < column_count ; vl++){ if($(row).childNodes[vl].firstChild.id.substring(0,7) == "new_val"){ values = values+"'"+$(row).childNodes[vl].firstChild.value+"'," ; }} values = values.substring(0,values.length - 1) ; values = values +")" ;
//	var pkName = "previous_value_"+rec_num+"_0" ;
//	var PK = $(pkName).value ;
	
		
	//var update_sql_string = "UPDATE `"+$('sel_database').value+"`.`"+$('sel_table').value+"` SET ("+$('columns').value+") VALUES "+values+" WHERE `"+$('first_column').value+"` = "+PK ;
	
	//  UPDATE `bikeshop`.`bla` SET `Bla3` = '25' WHERE `bla`.`Bla2` =0 LIMIT 1 ;
	
//	var eb = "save_button_"+rec_num ;
//	var updateRequest = new Request({
//		method: 'get',
//		url: 'modules/sql_action.php?query='+update_sql_string ,
//		onRequest: function(){
//			
//		},
//		onComplete: function(){
//			$(eb).src = "img/edit.png" ;
//			$(eb).addEvent('click', function(event){
//				edit_row(rec_num, eb);
//			});
//		}
//	}).send() ;
//}

function create_db(database_name){ // works
	new_db_str = "CREATE DATABASE `"+database_name+"`" ;
	var CreateDatabase = new Request({
		method: 'get',
		url: 'modules/sql_action.php?query='+new_db_str,
		onRequest: function(){
			$('new_db_name').value = '' ;
			dc();
		},
		onComplete: function(){
			get_databases() ;
			$('sel_database').value = database_name ;
			select_database(database_name) ;
		}
	}).send() ;
}  



function expand(database_name, parent_node, toggle_button){
	collapse_all_expanded() ;
	select_database(database_name, parent_node) ;
	$(toggle_button).set('src', 'img/min.png') ;
	$(toggle_button).set('onclick', 'collapse(\''+parent_node+'\',\''+database_name+'\',this.id)') ;
	
	//also make the database text onclick something like $().fireEvent('onclick')
	
}
var nmr = 0 ;
function collapse(object,database_name, toggle_button){  //<img onclick="collapse('database_td_4','bikeshop',this.id)" src="img/min.png" id="toggle_bikeshop"/>
	$(object).set('html', database_name) ;
	$(toggle_button).set('src', 'img/plus.png') ;
	$(toggle_button).set('onclick', 'expand(\''+database_name+'\',this.parentNode.nextSibling.id,this.id)') ;
}


function collapse_all_expanded(){
	var object = '' ;
	var x= 0 ;
	function create_elem(x){
		object = "database_td_"+x ;
		object = $(object) ;
		return object ;
	}
	var c = 1 ;
	while(create_elem(c)){
		try {
			if(object.firstChild.nextSibling){
				var new_elem = create_elem(c) ;
				var all_html = new_elem.innerHTML ;
				var dbn = all_html.substring(0,all_html.indexOf("<")) ;
				new_elem.set('html', dbn) ; //and change picture
				var expand_image = "toggle_"+dbn ;
				$(expand_image).src = "img/plus.png" ;
			}
		}catch(e){
			//alert('broke') ;
			break ;
		}
		c++ ;
	}
	
				
}

var tbl_num , previous, elem_text ;
function s_t(table_name, tbl_num){
	select_table(table_name) ;
	if($('selected_table_number').value > 0 ){ previous = "table_td_"+$('selected_table_number').value ; $(previous).style.backgroundColor = "#FFFFFF" ;	 }
	$('table_selected').value == true ;
	$('selected_table_number').value = tbl_num ;	
	elem_text = "table_td_"+tbl_num ;
	$(elem_text).style.backgroundColor = "red" ;
}
