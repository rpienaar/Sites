<?php
function db_con($DBH,$DBU,$DBP){
	try {
		$c = @mysql_connect($DBH,$DBU,$DBP);
		$_SESSION['db_connected'] = true;
		$_SESSION['db_host'] = $DBH;
		$_SESSION['db_user'] = $DBU;
		$_SESSION['db_pass'] = $DBP;
		return ($c);
	} catch (Exception $E) { return "Connection error";}
}
function db_con_close($DBCON){
	unset($_SESSION['db_pass']);
	unset($_SESSION['selected_database']);
	unset($_SESSION['selected_table']);
	mysql_close($DBCON);
	$_SESSION['db_connected'] = false;
}
?>