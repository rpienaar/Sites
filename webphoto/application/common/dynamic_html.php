<?php

class Dynamic_html {
  
  public function select($html_name_id,
                         $js_function,
                         $js_arguments,
                         $choose,
                         $array,
                         $option_array_value_string,
                         $option_array_string,
                         $selected_js_function,
                         $selected_js_arguments,
                         $normal_array=false,
                         $edit=false,
                         $edit_existing_val
                         ){
    $str = '';
    $str='<select name="'.$html_name_id.'" id="'.$html_name_id;
    if($js_function){
      $str.='" onchange="'.$js_function.'('.$js_arguments.');">';
    }else{
      $str.='" >';
    }
    if($choose){
      $str .= '<option>choose</option>';
    }
      $script = '';
      $selected = '';
      foreach($array as $item){
        $selected = '';
        
        if($normal_array){
          $compare_value = $item;
        }else{
          $compare_value = $item[$option_array_value_string];
        }
        $set_value = set_value($html_name_id);
        if($edit){
          if( set_value($html_name_id) > '' ){
            $set_value = set_value($html_name_id);
          }else{
            $set_value = $edit_existing_val;
          }
        }
        //if($compare_value == set_value($html_name_id) ){
        if($compare_value == $set_value ){
          $selected = ' selected="selected" ';
          if($selected_js_function && $selected_js_arguments){
            $script='<script type="text/javascript">'.$selected_js_function.'('.$selected_js_arguments.'); </script>';
          }
        }
        $str .= '<option '.$selected.' ';
        if($normal_array){
          $str .= 'value="'.$item.'">'.$item;
        }else{
          $str .= 'value="'.$item[$option_array_value_string].'">'.$item[$option_array_string];
        }
        $str .= '</option>';
        
      }
    return $str.'</select>'.$script;
  }
  
  
  public function search_select($id_name,$js_func,$js_arguments,$choose=true,$search_array){
    $str = '';
    $str .= '<select id="'.$id_name.'" name="'.$id_name.'" onchange="'.$js_func.'('.$js_arguments.');" >';
    if($choose){
      $str .= '<option>Choose</option>';
    }
    foreach($search_array as $col){
      $str .= '<option value="'.$col['name'].'">'.$col['label'].'</option>';
    }
    return $str.'</select>';
  }
  
  public function boolean_radio($name,$value=false){
    
    if($value){
      $compare_value = $value;
    }else{
      $compare_value = set_value($name);
    }
    
    $str = '';
      if($compare_value == 'true'){
        $yes_checked = ' checked="checked" ';
        $no_checked = '  ';
      }elseif($compare_value == 'false'){
        $yes_checked = '  ';
        $no_checked = ' checked="checked" ';
      }else{
        $yes_checked = ' checked="checked" ';
        $no_checked = '  ';
      }
    $str .= 'Yes<input type="radio" name="'.$name.'" value="true" '.$yes_checked.' />';
    $str .= 'No<input type="radio" name="'.$name.'" value="false" '.$no_checked.' />';
    return $str;
  }
  
}