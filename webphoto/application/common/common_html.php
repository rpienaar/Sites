<?php

class Common_html {
  
  public function title_select($name,$selected=false){
    $str='';
    foreach(array('Miss','Mr','Mrs','Ms') as $title){
      $sel = '';
      if($selected == $title){
        $sel = ' selected="selected" ';
      }
      $str .= '<option value="'.$title.'" '.$sel.' >'.$title.'</option>';
    }
    return '<select name="'.$name.'">'.$str.'</select>';
  }
  
}