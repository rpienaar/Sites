<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ctrl extends CI_Controller {
  
  public function __construct(){
		parent::__construct();
    $dynamic_header_file = 'public_header';
    $this->load->model('album_model');
  }
  
  public function view($offset=0){
    if ( ! file_exists('application/views/album/index.php') ){
      show_404();
    }
    if (!$this->tank_auth->is_logged_in()) {
			$data['authenticated'] = FALSE;
		}else{
			$data['authenticated'] = TRUE;
    }
    if (!$this->tank_auth->is_logged_in()) { 
			$dynamic_header_file = "public_header";
		}else{
      $dynamic_header_file = "header";
    }

    $data['albums'] = $this->album_model->listall($offset);
    $config['base_url'] = site_url("album");
    $config['total_rows'] = $this->album_model->count();
    $this->pagination->initialize($config);
    $data['album_pagination'] = $this->pagination->create_links();

    $arr=array();
		if(is_array($data['albums'])){
				foreach($data['albums'] as $alb){
					$arr[$alb['id']]=$this->album_model->album_photo_count($alb['id']);
				}
		}
		$data['album_counts'] = $arr;
    
    $this->load->view("template/".$dynamic_header_file,$data);
    $this->load->view("index",$data);
    $this->load->view("template/footer",$data);
  }
  

}