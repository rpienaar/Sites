<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Album_ctrl extends CI_Controller {

	var $logout_html = '';

  function __construct(){				
	  parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
    $this->load->model('album_model');
    $this->load->model('photo_model');
  }

	public function view($offset=0){
		if (!$this->tank_auth->is_logged_in()) {
			$data['authenticated'] = FALSE;
		}else{
			$data['authenticated'] = TRUE;
    }
    $data['albums'] = $this->album_model->listall($offset);
    $config['base_url'] = site_url("album");
    $config['total_rows'] = $this->album_model->count();
    $this->pagination->initialize($config);
    $data['album_pagination'] = $this->pagination->create_links();
		
		$arr=array();
		if(is_array($data['albums'])){
				foreach($data['albums'] as $alb){
					$arr[$alb['id']]=$this->album_model->album_photo_count($alb['id']);
				}
		}
		$data['album_counts'] = $arr;
		
    $this->load->view("template/header",$data);
    $this->load->view('album/index', $data);
    $this->load->view("template/footer",$data);
	}
  
  public function gallery($album_name){
		$album = $this->album_model->open_by_name($album_name);
		$album_id = $album[0]['id'];
		if( $album === FALSE){
				show_404();
		}else{
				$data['photos'] = $this->photo_model->album_photos($album_id);
				$data['album_id'] = $album_id;
				$data['album_name'] = $album_name;
				if( $data['photos'] !== FALSE ){
					$data['galleriffic_enabled']='true';
				}else{
					$data['galleriffic_enabled']='false';
				}
				$this->load->view("template/header",$data);
				$this->load->view('album/gallery', $data);
				$this->load->view("template/footer",$data);
		}
  }
  
  public function create(){
    $data['some']='';
    $this->form_validation->set_rules('album_name', 'Album name', 'required');
    $this->load->view("template/header",$data);
    if ($this->form_validation->run() == FALSE) {  
      $this->load->view('album/create', $data);
    } else {
      $id = $this->album_model->create();
      redirect('/album/'.$id, 'refresh'); 
    }
    $this->load->view("template/footer",$data);
  }	
  
}

?>