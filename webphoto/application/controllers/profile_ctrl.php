<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_ctrl extends CI_Controller {

  function __construct(){				
	  parent::__construct();
    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
    $this->load->model('user_model');
  }
  
  public function view(){
    $user_id = $this->tank_auth->get_user_id();
    $user = $this->user_model->open($user_id);
    $data['user'] = $user;
    
    $this->load->view("template/header",$data);
    $this->load->view('profile/index', $data);
    $this->load->view("template/footer",$data);
  }
  
}