<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Album_upload_ctrl extends CI_Controller {

	function __construct(){
		parent::__construct();
    
    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
    
    $this->load->model('album_model');
    $this->load->model('photo_model');
	}

	public function upload_form($album_name){
    $album_rec = $this->album_model->open_by_name($album_name);
    $data['album_name'] = $album_name;
    $data['album_id'] = $album_rec[0]['id'];
    $this->load->view("template/header",$data);
		$this->load->view('album_upload/index', array('error'=>''));
    $this->load->view("template/footer",$data);
	}
  
  public function upload(){
    $album_id   = $this->input->post('album_id');
    $album_name = $this->input->post('album_name');
    
    $data['album_id']   = $album_id;
    $data['album_name'] = $album_name;
    
    $upload_array = $this->uploadFiles($album_name,$album_id);
    if(is_array($upload_array)){
      //redirect('/album/'.$album_name, 'refresh');
    }else{
      $this->load->view("template/header",$data);
      $this->load->view('album_upload/index', array('error'=>$upload_array));
      $this->load->view("template/footer",$data);
    }
  }
  
  public function uploadFiles($album_name,$album_id){
    $file_array = array();
    $error=0;
    for( $i=0; $i < count($_FILES['photos']['name']); $i++ ){
      
      $now = time();
      $gmt = local_to_gmt($now);
      $_FILES['userfile']['name']     = trim_filename_stuff( $gmt ."_". $album_name ."_". $_FILES['photos']['name'][$i] );
      $_FILES['userfile']['type']     = $_FILES['photos']['type'][$i];
      $_FILES['userfile']['tmp_name'] = $_FILES['photos']['tmp_name'][$i];
      $_FILES['userfile']['error']    = $_FILES['photos']['error'][$i];
      $_FILES['userfile']['size']     = $_FILES['photos']['size'][$i];
      
      if(!($this->upload->do_upload())){
        $error++;
      }else{
        
        $photo_id = $this->photo_model->create_details($_FILES['userfile']['name'],$album_id);
        $this->album_model->check_cover_id($album_id,$photo_id);
        
        $src_img   = constant('SOURCE_IMG_DIR') . $_FILES['userfile']['name'];
        $thumb     = constant('THUMB_IMG_DIR')  . photo_thumb_name($_FILES['userfile']['name']);
        $album_img = constant('ALBUM_IMG_DIR')  . $_FILES['userfile']['name'];
        
        $this->do_resize($src_img,$thumb,75,75);
        //$this->do_resize($src_img,$album_img,500,544);
      }
    }
    if($error > 0){
      return $this->upload->display_errors('<p class="error">', '</p>');
    }else{
      return $file_array;
    }
  }
  
  public function do_resize($src_img,$target_path,$width,$height){
    $config_manip = array(
        'dynamic_output'  => 'FALSE',
        'library_path'    => '/usr/bin',
        'image_library'   => 'GD',
        'source_image'    => $src_img,
        'new_image'       => $target_path,
        'maintain_ratio'  => TRUE,
        'create_thumb'    => TRUE,
        //'thumb_marker'    => '_thumb',
        'width'           => $width,
        'height'          => $height
    );
    
    $this->load->library('image_lib', $config_manip);
    if (!$this->image_lib->resize()) {
        echo $this->image_lib->display_errors();
    }
    // clear //
    $this->image_lib->clear();
    return 0;
  }
  
}
?>