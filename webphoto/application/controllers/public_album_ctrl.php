<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_album_ctrl extends CI_Controller {

  function __construct(){				
	  parent::__construct();
    $dynamic_header_file = 'public_header';
    $this->load->model('album_model');
    $this->load->model('photo_model');
  }
  
  public function view(){
    $data['some']='some';
    if (!$this->tank_auth->is_logged_in()) { 
			$dynamic_header_file = "public_header";
		}else{
      $dynamic_header_file = "header";
    }
    $this->load->view("template/".$dynamic_header_file,$data);
    $this->load->view('public_album/index', $data);
    $this->load->view("template/footer",$data);
  }
  
  public function gallery($album_id){
    if (!$this->tank_auth->is_logged_in()) { 
			$dynamic_header_file = "public_header";
		}else{
      $dynamic_header_file = "header";
    }
    $album = $this->album_model->open_by_name($album_id);
		if( $album === FALSE){
				show_404();
		}else{
      $data['photos'] = $this->photo_model->album_photos($album_id);
      $data['album_id'] = $album_id;
			if( $data['photos'] !== FALSE ){
				$data['galleriffic_enabled']='true';
			}else{
				$data['galleriffic_enabled']='false';
      }
		}
    $this->load->view("template/".$dynamic_header_file,$data);
    $this->load->view('public_album/gallery', $data);
    $this->load->view("template/footer",$data);
  }
  
}