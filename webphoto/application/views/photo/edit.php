
<form method="post" action="<?php echo base_url("photo/save"); ?>" >
  <table>
    <tr>
      <td>Title</td><td><input type="text" name="title" value="<?php echo $photo_title; ?>" /></td>
    </tr>
    <tr>
      <td>Description</td><td><input type="text" name="description" value="<?php echo $photo_description; ?>" /></td>
    </tr>
    <tr>
      <td>
        <input type="submit" value="Save" />
      </td>
    </tr>
  </table>
  <input type="hidden" name="photo_id" value="<?php print $photo_id; ?>" />
</form>

<br/>

<a id="make_cover" href="#">Make album cover image</a> | 

<a id="del" href="#" >Delete Photo</a>
<br />

<br/>

<img src="<?php echo base_url('uploads/'.$photo_name); ?>">

<script type="text/javascript">
  
  $('#make_cover').bind('click',function(){
    if(confirm('Make cover?')==true){
      location.href = '<?php echo base_url('photo/album_cover_id/'.$photo_id); ?>';
    }
  });
  
  $('#del').bind('click',function(){
    if(confirm('Delete photo?')==true){
      location.href = '<?php echo base_url('photo/delete/'.$photo_id); ?>';
    }
  });
  
</script>