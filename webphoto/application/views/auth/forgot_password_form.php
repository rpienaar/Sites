<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
?>
<?php echo form_open($this->uri->uri_string()); ?>
<table cellpadding="2" cellspacing="2" >
  <tr>
    <td colspan="3">
      <h2>
        Forgot Password
      </h2>
    </td>
  </tr>
	<tr>
	<tr>
		<td><?php echo form_label($login_label, $login['id']); ?></td>
		<td><?php echo form_input($login); ?></td>
		<td style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></td>
	</tr>
  <tr>
    <td colspan="3" >
      <?php echo form_submit('reset', 'Get a new password'); ?>
    </td>
  </tr>
  <tr>
    <td colspan="3" >
      <?php echo anchor('/auth/login', 'Login'); ?>
    </td>
  </tr>
  <tr>
    <td  colspan="3">
      <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?>
    </td>
	</tr>
</table>

<?php echo form_close(); ?>