<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Album_model extends CI_Model {
  
  public function __construct(){
  }
  
  public function all(){
    $this->db->order_by('timestamp','asc');
    $query = $this->db->get('album');
    return $query->result_array();
  }
  
  public function count(){
    return $this->db->count_all("album");
  }

  public function open($id){
    $query = $this->db->get_where('album',array('id' => $id),1);
    if( $query->num_rows > 0 ){
      return $query->result_array();
    }else{
      return FALSE;
    }
  }
  
  public function open_by_name($name){
    $query = $this->db->get_where('album',array('name' => $name),1);
    if( $query->num_rows > 0 ){
      return $query->result_array();
    }else{
      return FALSE;
    }
  }
  
  public function listall($offset){
    $this->db->select("album.*, photo.name as `cover_name`");
    $this->db->from("album");
    $this->db->join("photo", "album.cover_photo_id = photo.id ", 'left outer');
    $query = $this->db->get('',20,$offset);
    return $query->result_array();
  }
  
  public function create(){
    $name = $this->input->post('album_name');
    $data = array(
      'name' => $name
    );
    $this->db->insert('album', $data);
    return $name;
  }

  public function update($id){
   $data = array(
      'name' => $this->input->post('name')
    );
    $this->db->where('name',$id);
    return $this->db->update('album',$data);
  }
  
  public function delete($asset_category_id){
    //$this->db->delete('XXX',array('id'=>$id));
  }
  
  public function check_cover_id($album_id,$photo_id){
    $query = $this->db->get_where('album',array('id' => $album_id),1);
    $result = $query->result_array();
    if( $result[0]['cover_photo_id'] == 0 ){
        $data = array(
                 'cover_photo_id' => $photo_id
              );
      $this->db->where('id', $album_id);
      $this->db->update('album', $data); 
    }
  }
  
    public function save_cover($photo_id,$album_id){
    $data = array(
      'cover_photo_id' => $photo_id
    );
    $this->db->where('id',$album_id);
    return $this->db->update('album',$data);
  }
  	
	public function album_photo_count($album_id){
    $this->db->select("*");
    $this->db->from("album");
    $this->db->join("photo", "album.id = photo.album_id ");
    $query = $this->db->get_where('','album.id = '.$album_id);;
    return $query->num_rows;
	}
  
  
}