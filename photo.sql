-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 28, 2013 at 09:06 PM
-- Server version: 5.1.68
-- PHP Version: 5.3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webphoto`
--

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) DEFAULT '',
  `description` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `album_id`, `name`, `timestamp`, `title`, `description`) VALUES
(1, 15, 'Test4kHh7kT.jpg', '2013-04-18 21:14:09', 'a', 'b'),
(2, 15, 'Test13-1.jpg', '2013-04-18 21:14:10', 'title', 'description'),
(3, 15, 'Test63547_638977082798415_660914194_n.jpg', '2013-04-18 21:14:10', 'title', 'description'),
(4, 17, 'test2221752_544918602219315_2049297718_n.jpg', '2013-04-18 22:18:52', 'title', 'description'),
(5, 17, 'test2309023_543134329064409_657678982_n.jpg', '2013-04-18 22:18:52', 'title', 'description'),
(6, 17, 'test2576719_306410706154425_1377397821_n.jpg', '2013-04-18 22:18:52', 'title', 'description'),
(7, 18, 'mmm13-1.jpg', '2013-04-28 15:29:47', 'title', 'description'),
(8, 18, 'mmm466758_326899797438849_280582503_o.jpg', '2013-04-28 15:29:47', 'title', 'description'),
(9, 18, 'mmm472986_327002517428577_2103070761_o.jpg', '2013-04-28 15:29:47', 'title', 'description'),
(10, 18, 'mmmhappy-quotes-inspirational-quotes.jpg', '2013-04-28 15:29:48', 'title', 'description'),
(11, 18, 'mmmNever-Give-Up.jpeg', '2013-04-28 15:29:48', 'title', 'description'),
(12, 19, 'mmm24kHh7kT.jpg', '2013-04-28 15:31:25', 'title', 'description'),
(13, 20, 'mmm313-1.gif', '2013-04-28 15:31:44', 'title', 'description'),
(14, 21, 'mmm421180_427808043982039_489768975_n.jpg', '2013-04-28 15:31:57', 'title', 'description'),
(15, 22, 'another263547_638977082798415_660914194_n.jpg', '2013-04-28 15:32:10', 'title', 'description'),
(16, 23, 'mmm568.jpg', '2013-04-28 15:32:27', 'title', 'description'),
(17, 38, 'aAre-you-bored.jpg', '2013-04-28 19:19:04', '', ''),
(18, 39, 'b309023_543134329064409_657678982_n.jpg', '2013-04-28 19:54:47', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
