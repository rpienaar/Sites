<?php
check_dependancy("common.php");

class group extends common {
	static $nav_page = 'admin_groups.php';
	private $groupname;
	private $groupid;
	private $pageid;
	private $buttonid;
	static $dbcon;
	
	function group(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
  	group::$dbcon = $db;
	}
	
	function add_group($groupname){
		group::$dbcon->query("INSERT INTO `group` (`Group`) VALUES ('".$groupname."')");
		//common::nav(group::$nav_page);
	}
	function edit_group(){
		
	}
	
	function delete_group($groupid){
		group::$dbcon->query("DELETE FROM `group` WHERE `GroupId` = '".$groupid."' ");
		group::$dbcon->query("DELETE FROM `group_buttons` WHERE `GroupId` = '".$groupid."' ");
		group::$dbcon->query("DELETE FROM `group_page` WHERE `GroupId` = '".$groupid."' ");
		//group::$dbcon->query("UPDATE `user` SET `GroupId` = '3' WHERE `GroupId` = '".$groupid."'"); // ?????????????
	}
	
	function add_group_button($groupid,$buttonid){
		$btn = group::$dbcon->get_row("SELECT * FROM `group_buttons` WHERE `GroupId` = '".$groupid."' AND `ButtonId` = '".$buttonid."' ");
		if(empty($btn->GroupButtonId)){
			group::$dbcon->query("INSERT INTO `group_buttons` (`GroupId`,`ButtonId`) VALUES (".$groupid.",".$buttonid.")");
			//common::nav(group::$nav_page);
		}else{
			return "<br /><font color=\"red\">Button already added !</font>";
		}		
	}
	
	function delete_group_button($groupid,$buttonid){
		group::$dbcon->query("DELETE FROM `group_buttons` WHERE `GroupId` = '".$groupid."' AND `ButtonId` = '".$buttonid."' ");
	}
	
	function add_group_page($groupid,$pageid){
		$page = group::$dbcon->get_row("SELECT * FROM `group_page` WHERE `GroupId` = '".$groupid."' AND `PageId` = '".$pageid."' ");
		if(empty($page->GroupPageId)){
			group::$dbcon->query("INSERT INTO `group_page` (`GroupId`,`PageId`) VALUES ('".$groupid."','".$pageid."')");
			//common::nav(group::$nav_page);
		}else{
			return "<br /><font color=\"red\">Page already added !</font>";
		}
	}
	
	function delete_group_page($groupid,$pageid){
		group::$dbcon->query("DELETE FROM `group_page` WHERE `GroupId` = '".$groupid."' AND `PageId` = '".$pageid."' ");
	}
	
	function open_group($groupid){
		$group = group::$dbcon->get_row("SELECT * FROM `group` WHERE `GroupId` = '".$groupid."' ");
		return $group;
	}
	
	function open_group_by_groupname($groupname){
	        $group = group::$dbcon->get_row("SELECT * FROM `group` WHERE `Group` = '".$groupname."' ");
	        return $group;       
	}
	
	function listall_groups($limit){
		$groups = group::$dbcon->get_results("SELECT * FROM `group` ");
		return $groups;
	}
	
	function group_count(){
		$group_count = group::$dbcon->get_var("SELECT count(*) FROM `group` ");
		return $group_count;
	}
	
	function group_user_count($groupid){
		$group_user_count = group::$dbcon->get_var("SELECT count(*) FROM `group` JOIN `user` ON (`group`.`GroupId` = `user`.`GroupId`) where `group`.`GroupId` = '".$groupid."' ");
		return $group_user_count;
	}
	
	function view_group_and_users($groupid){
		$view_group_user = group::$dbcon->get_results("SELECT `group`.`GroupId`, `group`.`Group`, `user`.`UserId`, `user`.`Username`, `userdetails`.`MSISDN`
		                                               FROM `group` JOIN `user` ON ( group.GroupId = user.GroupId )
		                                               JOIN `userdetails` ON (user.UserId = userdetails.UserId) 
		                                               WHERE `group`.`GroupId` = '".$groupid."' ");                      
		return $view_group_user;
	}
	
	function view_group($groupid){
		return $this->view_group_db(group::$dbcon,$groupid);
	}
	
	function view_group_db($db,$groupid){
		$UserSQL   = "SELECT user.Username FROM `group`
		              JOIN `user` ON ( `group`.`groupId` = `user`.`groupId` )
		              WHERE `group`.`groupId` = '".$groupid."' ";
		              
		$ButtonSQL = "SELECT button.Id, button.text FROM `group`
		              JOIN `group_buttons` ON ( `group`.`GroupId` = `group_buttons`.`Groupid` )
		              JOIN `button`        ON ( `group_buttons`.`ButtonId` = `button`.`Id` )
		              WHERE `group`.`groupId` = '".$groupid."' ";
		              
		$PageSQL   = "SELECT page.PageId, page.PageName FROM `group`
		              JOIN `group_page` ON ( `group`.`GroupId`     = `group_page`.`GroupId` )
		              JOIN `page`       ON ( `group_page`.`PageId` = `page`.`PageId` )
		              WHERE `group`.`groupId` = '".$groupid."' ";
		
		$group_users   = $db->get_results($UserSQL);
		$group_buttons = $db->get_results($ButtonSQL);
		$group_pages   = $db->get_results($PageSQL);
		
		$view_group_array = array();
		
		$view_group_array['users']   = $group_users;
		$view_group_array['buttons'] = $group_buttons;
		$view_group_array['pages']   = $group_pages;
		
		return $view_group_array;
	}
	
}
/*
if(!empty($_POST)){
	if(@$_POST['group_action'] == 'group'){
		if(!empty($_POST['groupName'])){
			$g->add_group($_POST['groupName']);
		}
	}elseif(@$_POST['group_action'] == 'del_group'){
		if(!empty($_POST['gi'])){
			$g->delete_group($_POST['gi']);
		}
	}elseif(@$_POST['group_action'] == 'del_page_group'){
		if(!empty($_POST['gi']) && !empty($_POST['pi'])){
			$g->delete_group_page($_POST['gi'],$_POST['pi']);
		}
	}elseif(@$_POST['group_action'] == 'add_page'){
		if(!empty($_POST['groupPageName']) && !empty($_POST['pagename'])){
			$page_add_error = $g->add_group_page($_POST['groupPageName'], $_POST['pagename']);
		}
	}elseif(@$_POST['group_action'] == 'add_button'){
		if(!empty($_POST['btn_group'])&&!empty($_POST['buttong'])){
			$g->add_group_button($_POST['btn_group'], $_POST['buttong']);
		}
	}elseif(@$_POST['group_action'] == 'del_group_button'){
		if(!empty($_POST['gi']) && !empty($_POST['bi'])){
			$g->delete_group_button($_POST['gi'],$_POST['bi']);
		}
	}
}
*/
?>