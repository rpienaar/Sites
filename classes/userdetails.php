<?php
check_dependancy('pages.php');

class userdetails {
	static $dbcon;
	private $userid;
	
	function userdetails(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
		userdetails::$dbcon = $db;
	}
	
	function get_last_page_by_userid($userid){ // FIX THIS
		$lastpage = userdetails::$dbcon->get_var("SELECT `page`.`PageName` FROM `user`
		                                          JOIN `userdetails`  ON ( `user`.`UserId` = `userdetails`.`UserId` )
		                                          JOIN `page`         ON ( `userdetails`.`LastPageId` = `page`.`PageId` )
		                                          WHERE `user`.`UserId` = '".$userid."' ");
		return $lastpage;
	}
	
	function open_userdetails($userid){
		return $this->open_userdetails_db($userid,userdetails::$dbcon);
	}
	function open_userdetails_db($userid,$db){
		$my_settings = $db->get_row("SELECT 
																`userdetails`.`UseBackground`,
																`userdetails`.`Opacity`,
																`userdetails`.`TrsButtons`,
																`userdetails`.`PlaylistId`,
																`userdetails`.`AvatarId`,
																`playlist`.`PlaylistURL`,
																`file`.`Name`
																FROM `user` 
																JOIN `userdetails`	ON ( `user`.`UserId`						= `userdetails`.`UserId` )
																JOIN `playlist` 		ON ( `userdetails`.`PlaylistId`	= `playlist`.`PlaylistId` )
																JOIN `file` 				ON ( `userdetails`.`AvatarId`		= `file`.`FileId` )
																WHERE 
																`user`.`UserId` = '".$userid."' ");
		return $my_settings;
	}

	function set_user_last_page($pageid,$userid){
		userdetails::$dbcon->query("UPDATE `userdetails` SET `LastPageId` ='".$pageid."' WHERE `UserId` = '".$userid."' ");
		$pageName = pages::get_page_name_by_pageid($pageid);
		$_SESSION['lastpage'] = $pageName;
	}
	
	function update_verification_code($userid,$verification_code){
		$this->update_verification_code_db($userid,$verification_code,userdetails::$dbcon);
	}
	function update_verification_code_db($userid,$verification_code,$db){
		$db->query(" UPDATE `userdetails` SET `VerificationCode` = '".$verification_code."' WHERE `UserId` = '".$userid."' ");
	}
	
}

?>
