<?php

class dbif extends common {
	static $dbcon;
	
	function dbif(){
		$dbclass = new cdbcon();
		$db = $dbclass->return_db_con();
		dbif::$dbcon = $db;
	}
	
	function listall_databases(){ // ->Database
		$databases = dbif::$dbcon->get_results(" SHOW DATABASES ");
		return $databases;
	}
	
	function add(){
		//return dbif::$dbcon->debug();
	}
	
	function edit(){
		//return dbif::$dbcon->debug();
	}
	
	function delete(){
		//return dbif::$dbcon->debug();
	}
	
	function open($table,$column,$value){
		return dbif::$dbcon->get_row("SELECT * FROM `".$tablename."` WHERE `".$column."` = '".$value."' ");
		//return dbif::$dbcon->debug();
	}
	
	function listall($tablename){
		return dbif::$dbcon->get_results("SELECT * FROM `".$tablename."` ");
		//return dbif::$dbcon->debug();
	}
	
	function listall_clause($tablename,$clause_sql){
		return dbif::$dbcon->get_results("SELECT * FROM `".$tablename."` ".$clause_sql);
		//return dbif::$dbcon->debug();
	}
	
	function listall_offset($tablename,$offset,$limit){
		return dbif::$dbcon->get_results("SELECT * FROM `".$tablename."` LIMIT ".$limit." OFFSET ".$offset);		
		//return dbif::$dbcon->debug();
	}
	
	function listall_offset_clause($tablename,$offset,$limit,$clause_sql){
		return dbif::$dbcon->get_results("SELECT * FROM `".$tablename."` ".$clause_sql."  LIMIT ".$limit." OFFSET ".$offset);
		//return dbif::$dbcon->debug();
	}
	
	function table_count($tablename){
		return dbif::$dbcon->get_var("SELECT count(*) FROM `".$tablename."` ");
		//return dbif::$dbcon->debug();
	}
	
	function table_count_clause($tablename,$sql_clause){
		return dbif::$dbcon->get_var("SELECT count(*) FROM `".$tablename."` ".$sql_clause);
		//return dbif::$dbcon->debug();
	}
	
	function table_columns($tablename){
		$result = array();
		$column_results = dbif::$dbcon->get_results(" SHOW COLUMNS FROM ".$tablename);
		//return dbif::$dbcon->debug();
		foreach($column_results as $column_row){
			array_push($result,$column_row->Field);
		}
		return $result;
	}

  function query($QueryString){
		return dbif::$dbcon->query($QueryString);
	}
	
	function get_row($QueryString){
		return dbif::$dbcon->get_row($QueryString);
	}
	
	function get_results($QueryString){
		return dbif::$dbcon->get_results($QueryString);
	}
	
	function debug(){
		return dbif::$dbcon->debug();
	}
	
}

?>