<?php
/** 
 * Google Weather 
 * 
 * A simple class to get weather info through Google Api
 * 
 * Features 
 *  
 *    -    Gets weather data for a city and displays it
 * 
 * License 
 *  
 * Copyright (c) 2011, Amit YAdav <amityadav@amityadav.name> 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without  
 * modification, are permitted provided that the following conditions are met: 
 *  
 *    -    Redistributions of source code must retain the above copyright notice,  
 *        this list of conditions and the following disclaimer. 
 * 
 *    -    Redistributions in binary form must reproduce the above copyright  
 *        notice, this list of conditions and the following disclaimer in the  
 *        documentation and/or other materials provided with the distribution. 
 * 
 *    -    Neither the name of Made Media Ltd. nor the names of its contributors  
 *        may be used to endorse or promote products derived from this  
 *        software without specific prior written permission. 
 * 
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  
 *    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  
 *    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR  
 *    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR  
 *    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  
 *    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,  
 *    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR  
 *    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  
 *    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  
 *    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS  
 *    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 * 
 * Limitations 
 *  
 * The class contains some cities, one can change the city array and use this
 * class for their countries array
 * 
 * 
 * @category    Utilities 
 * @package     Google Weather 
 * @author      Amit YAdav <amityadav@amityadav.name> 
 * @copyright   2011 Amit Yadav 
 * @version     0.1 
 * @website     http://amityadav.name 
 */ 
	 
class googleWeather{
	//var $cities = array("Pune", "Mumbai", "Noida", "Chennai", "Calcutta", "Alabama", "Alaska","Arizona", "Arkansas", "California", "Colorado", "Connecticut",  "Delaware",  "District Of Columbia",  "Florida",  "Georgia",  "Hawaii",  "Idaho",  "Illinois",  "Indiana",  "Iowa",  "Kansas",  "Kentucky",  "Louisiana",  "Maine",  "Maryland",  "Massachusetts",  "Michigan",  "Minnesota",  "Mississippi",  "Missouri",  "Montana", "Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio",  "Oklahoma",  "Oregon",  "Pennsylvania",  "Rhode Island",  "South Carolina",  "South Dakota","Tennessee",  "Texas",  "Utah",  "Vermont",  "Virginia",  "Washington",  "West Virginia",  "Wisconsin",  "Wyoming");
	var $cities = array("johannesburg","Cape Town","Durban","Germiston","Pretoria","Port Elizabeth","Bloemfontein","East London","Vanderbijlpark","Pietermaritzburg","Polokwane","Rustenburg","Witbank","Klerksdorp");

	var $xml;
	var $selectedCity;

	function __construct($city){
		$this->selectedCity = $city;
	}

	function getWeatherInfo(){
		$this->xml = simplexml_load_file('http://www.google.com/ig/api?weather=' . $this->selectedCity);
	}


	function display_weather(){
		$forecastInformation = $this->xml->xpath("/xml_api_reply/weather/forecast_information/*");
		$currentConditions   = $this->xml->xpath("/xml_api_reply/weather/current_conditions");
		$currentConditions   = $currentConditions[0];
  ?>
		<div class="widget">
			<center>
				<span class="day">Today</span><br/>
				<?php echo date("M d")?><br/><br/><br/>

				<?php echo "<img class='weatherImage' src='http://www.google.com" . $currentConditions->icon->attributes() . "'>";?><br/><br/><br/>

				<?php echo $currentConditions->condition->attributes()?><br/><br/><br/>

				<span class="big"><?php echo $currentConditions->temp_f->attributes()?><span>�</span> F / <?php echo $currentConditions->temp_c->attributes()?><span>�</span> C</span><br/><br/>
				<?php echo $currentConditions->humidity->attributes()?><br/>
				<?php echo $currentConditions->wind_condition->attributes()?><br/>
			</center>
		</div>

  <?php
		$forecastConditions = $this->xml->xpath("/xml_api_reply/weather/forecast_conditions");
                $k=1;
		foreach ($forecastConditions as $forecastCondition) {
  ?>
			<div class="widget">
				<center>
					<span class="day"><?php echo $forecastCondition->day_of_week->attributes()?></span><br/>
					<?php echo date("M d", (time() + 60*60*24* $k++) )?><br/><br/><br/>

					<?php echo "<img class='weatherImage' src='http://www.google.com" . $forecastCondition->icon->attributes() . "'>";?><br/><br/><br/>

					<?php echo $forecastCondition->condition->attributes()?><br/><br/><br/>

					<span class="big"><?php echo number_format((($forecastCondition->high->attributes() - 32) * 5/9), 1)?><span>�</span></span>   LO <?php echo number_format((($forecastCondition->low->attributes() - 32) * 5/9), 1)?><span>�</span>
				</center>
			</div>
			
  <?php
		}
	}
}
  ?>