<?php
check_dependancy("common.php");
check_dependancy("cscope.php");

define('UPLOAD_ERROR_HTML',"<font color='red'><b>UPLOAD ERROR </b></font><br />");

class cfile extends common {
	private $fileid;
  private $file;
  private $allowed_types;
  private $upload_type;
  private $nav_page;
  private $filename;
  private $data;
  private $cdate;
  static $upload_error_html = "<font color='red'><b>UPLOAD ERROR </b></font><br />";
  private $file_type;
  private $MIN;
  private $MAX;
  static $dbcon;
	
	function cfile(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
		cfile::$dbcon = $db;
	}
	
	function create_file($filename,$data){
		$fh = fopen($filename, 'w') or die("can't open file");
		fwrite($fh,$data);
		fclose($fh);
		return true; // TODO: build a try catcher !!!
	}

	function upload_multiple_images($_POST,$cdate,$tab_nmr,$tg,$upload_date){
		$return_array = array();
		$ALLOWED_IMAGE_UPLOADS = $this->get_allowed_images();
		for($c = 0; $c < $_POST['upload_sel']; $c++){
			if(isset($_FILES['file'.$c]["name"])){
				$new_file_id = $this->upload_image(@$_FILES['file'.$c],$ALLOWED_IMAGE_UPLOADS,$cdate,$tab_nmr,$tg,$upload_date);
				$return_array[$c] = $new_file_id;
			}
  	}

		$all_failed=1;
		foreach($return_array as $ret){
			if( $ret != 0 ){
				$all_failed=0;
			}
		}
		if($all_failed == 1){
			return 0;
		}else{
			return $return_array;
		}
		
	}
	
  function upload_image($file,$allowed_types,$cdate,$tab_nmr,$tg,$upload_date){
		$fileName       = $file["name"];
    $filetype       = $file["type"];
    $temp_file_name = $file["tmp_name"];
    $fileExtension = strtolower(common::file_extension($fileName));
		if(in_array($fileExtension,$allowed_types)){
    	$name = constant('ACTIVE_PATH').constant('UPLOAD_FOLDER').'/'.str_ireplace(" ","%20",$fileName);
			$file_id = $this->create_file_record($fileName,$filetype,$cdate,constant('SCOPE_PUBLIC'),$upload_date);
    	if($file_id != false){
    		if($this->upload_file($temp_file_name,$name)){
    			$filename_only = common::file_name_without_extension($fileName);
    			$thumbnail = constant('ACTIVE_PATH').constant('UPLOAD_FOLDER').'/thumbs/'.$filename_only."_thumb.".$fileExtension;
    			if($tg->generate($name,250,190,$thumbnail)){
    				return $file_id;
    			}else{ return false; }
    		}else{ return false; }
    	}else{ return false; }
  	}else{ return false; }
  }
  
  function upload_avatar($file,$cdate,$log,$tg,$theme,$upload_date){
	  $fileName       = $file["name"];
    $filetype       = $file["type"];
    $temp_file_name = $file["tmp_name"];    
  	$allowed = $this->get_allowed_images();
  	$fileExtension = common::file_extension($fileName);
		if($this->validate_upload_type($fileExtension,$allowed)){
			$name = constant('ACTIVE_PATH').constant('UPLOAD_FOLDER').'/avatars/'.str_ireplace(" ","%20",$fileName);
			$filename_only = common::file_name_without_extension($fileName);		
			$thumbnail = constant('ACTIVE_PATH').constant('UPLOAD_FOLDER').'/avatars/'.$filename_only."_thumb.".$fileExtension;
			$file_id = $this->create_file_record($fileName,$filetype,$cdate,constant('SCOPE_PRIVATE'),$upload_date);
    	if($file_id != false){
				if($this->upload_file($temp_file_name,$name)){
					$avatar_id = cfile::$dbcon->insert_id;
					$log->update_avatar($avatar_id);
					if($tg->generate($name,250,190,$thumbnail)){
						$theme->set_session_data($_SESSION['FID'],$_SESSION['fname'],$_SESSION['theme'],$_SESSION['opacity'],$_SESSION['trans_buttons'],$fileName);
						return $file_id;	
		    	}else{ return false; }
				}else{ return false; }
			}else{ return false; }
		}else{ return false; }
  }
  
  function upload_video($file,$allowed_types,$cdate){
    $fileName       = $file["name"];
    $filetype       = $file["type"];
    $temp_file_name = $file["tmp_name"];
    $name = constant('ACTIVE_PATH').'video/'.$fileName;
    $fileExtension = common::file_extension($fileName);
    if(in_array($fileExtension,$allowed_types)){
    	$this->upload_file($temp_file_name,$name);
    	//common::nav("videos.php");
    }else{
    	return false;
    }
  }
  
  function upload_file($temp_file_name,$name){
    if( move_uploaded_file($temp_file_name,$name) ){
    	return true;
    }else{
    	return false;
    }
  }
  
  function delete_image($filename,$tab_nmr){ //if the file thats being deleted was someones background, then update the user to default background , also check avatar
  	$filename_only = common::file_name_without_extension($filename);
	  $fileExtension = common::file_extension($filename);
    @unlink(constant('ACTIVE_PATH').constant('UPLOAD_FOLDER').'/thumbs/'.$filename_only."_thumb.".$fileExtension);
    @unlink(constant('ACTIVE_PATH').constant('UPLOAD_FOLDER').'/'.$filename_only.".".$fileExtension);
    $filerec = cfile::$dbcon->get_row("SELECT `FileId` FROM `file` WHERE `Name` = '".$filename."' ");
    $background = cfile::$dbcon->get_row("SELECT * FROM `background` WHERE `FileId` = '".$filerec->FileId."' ");
    cfile::$dbcon->query("DELETE FROM `background` WHERE `FileId` = '".$filerec->FileId."' ");
    $this->delete_file_record($filename);
    //common::navparams("upload.php","?r=".$tab_nmr);
  }
  
  function delete_video($filename){
  	//common::nav("videos.php");
  }
  
  function delete_file($id){
  	cfile::$dbcon->query("DELETE FROM `file` WHERE `FileId` = '".$id."' ");
  }
  
  function update_file($filename){
  
  }
  
  function get_files_by_type($type){
    $files = cfile::$dbcon->get_results("SELECT * FROM `file` WHERE `Type` LIKE '".$type."%' ");
    return $files;
  }
  
  function create_file_record($name,$type,$cdate,$scope_id,$upload_date){  
  	if(cscope::validate_scope($scope_id)){
			
			if(!empty($name) && !empty($type)){
				$name = addslashes($name);
				$type = addslashes($type);
				if($upload_date != false){
					$SQL = "INSERT INTO `file` (`Name`,`Type`,`UploadDate`,`ScopeId`) VALUES ('".$name."','".$type."','".$upload_date."',".$scope_id.")";
				}else{
					$date = $cdate->date_str();
					$SQL = "INSERT INTO `file` (`Name`,`Type`,`UploadDate`,`ScopeId`) VALUES ('".$name."','".$type."','".$date."',".$scope_id.")";
				}
				cfile::$dbcon->query($SQL);
				cfile::$dbcon->debug();
				$inserted_id = cfile::$dbcon->insert_id;
				if($inserted_id){ return $inserted_id;  }
				else{             return false; }
			}	
  	}else{
  		return false;
  	}
	}
	
	function delete_file_record($filename){
		cfile::$dbcon->query("DELETE FROM `file` WHERE `Name` = '".$filename."' ");
	}
	
	function listall_file_by_type($file_type,$MIN,$MAX){
		$filtered_files = cfile::$dbcon->get_results("SELECT * FROM `file` WHERE `Type` LIKE '%".$file_type."%' LIMIT ".$MIN.",".$MAX." ");
		return $filtered_files;
	}
	
	function listall_file_by_type_and_scope($file_type,$MIN,$MAX,$scope_id){
		if(cscope::validate_scope($scope_id)){
			$filtered_files = cfile::$dbcon->get_results("SELECT * FROM `file` WHERE `Type` LIKE '%".$file_type."%' AND `ScopeId` = '".$scope_id."' ORDER BY `FileId` DESC LIMIT ".$MIN.",".$MAX." ");
			return $filtered_files;
		}else{
			return false;
		}
	}
	
  function get_file_record_count($file_type,$scope_id){
  	if(cscope::validate_scope($scope_id)){
  		$Count = cfile::$dbcon->get_var("SELECT count(*) FROM `file` WHERE `Type` LIKE '%".$file_type."%' AND `ScopeId` = '".$scope_id."' ");
  		return $Count;
  	}else{
  		return false;
  	}
  }
 
 // TODO : implement this everywhere  needed !!!!!!!!!!!!!!
 function get_thumbnail_image_name($name){
 		$fileExtension = common::file_extension($name);
		$thumbname = common::file_name_without_extension($name)."_thumb.".$fileExtension;
		return $thumbname;
 }
 
 function get_allowed_images(){
		$ALLOWED_IMAGE_UPLOADS = array('jpg','bmp','gif','jpeg','png');
		return $ALLOWED_IMAGE_UPLOADS;
 }
 
 function validate_upload_type($uploading,$allowed){
 		if(in_array($uploading,$allowed)){ return true;}
 		else{                              return false;}
 }
 
 function open_file($fileid){
 		$file = cfile::$dbcon->get_row("SELECT * FROM `file` WHERE `FileId` = ".$fileid);
 		return $file;
 }
  
	function generate_upload_inputs($upload_amount,$r){
		for($ab=0; $ab < $upload_amount; $ab++){ print $r->cse("input","class='btn'|type='file'|name='file".$ab."'").$r->cse("br",""); }
	}
	
	function create_page(){
		
	}
	
}





















class thumbnailGenerator{
	var $allowableTypes = array(
		IMAGETYPE_GIF,
		IMAGETYPE_JPEG,
		IMAGETYPE_PNG
	);

	public function imageCreateFromFile($filename,$imageType){
    switch($imageType){
			case IMAGETYPE_GIF  :return imagecreatefromgif($filename);
			case IMAGETYPE_JPEG :return imagecreatefromjpeg($filename);
			case IMAGETYPE_PNG  :return imagecreatefrompng($filename);
			default             :return false;
		}
  }

	public function generate($sourceFilename,$maxWidth,$maxHeight,$targetFormatOrFilename = 'jpg'){
		$size = getimagesize($sourceFilename);
		if(!in_array($size[2],$this->allowableTypes)){ return false; }
			$pathinfo = pathinfo($targetFormatOrFilename);
			
			if($pathinfo['basename'] == $pathinfo['filename']){
				$extension = strtolower($targetFormatOrFilename);
					$targetFormatOrFilename = null;
			}else{
				$extension = strtolower($pathinfo['extension']);
			}
			
			switch($extension){
				case 'gif' : $function = 'imagegif' ; break;
				case 'png' : $function = 'imagepng' ; break;
				default    : $function = 'imagejpeg'; break;
			}
			
			$source = $this->imageCreateFromFile($sourceFilename, $size[2]);
			if(!$source){ return false; }
			
			if($targetFormatOrFilename == null){
				if($extension == 'jpg'){ header("Content-Type: image/jpeg"); }
				else{ header("Content-Type: image/$extension");
				}
			}
			
			if($size[0] <= $maxWidth && $size[1] <= $maxHeight){
				$function($source, $targetFormatOrFilename);
			}else{
				$ratioWidth = $maxWidth / $size[0];
				$ratioHeight = $maxHeight / $size[1];
				if($ratioWidth < $ratioHeight){
					$newWidth = $maxWidth;
					$newHeight = round($size[1] * $ratioWidth);
				}else{
					$newWidth = round($size[0] * $ratioHeight);
					$newHeight = $maxHeight;
				}
				$target = imagecreatetruecolor($newWidth, $newHeight);
				imagecopyresampled($target, $source, 0, 0, 0, 0, $newWidth, $newHeight, $size[0], $size[1]);
				$function($target, $targetFormatOrFilename);
			}
			
	return true;
	
	}
	
}



// TODO: this is incorrect, i should instanciate it on my own !!!!!!!!!

$cfile = new cfile();
$tg = new thumbnailGenerator();
?>