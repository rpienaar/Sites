<?php
check_dependancy("common.php");

class button extends common {
	public $buttonId;
	public $button_html_code;
	public $button_name;
	public $button_items;
	public $button_group;
	public $new_group;
	static $dbcon;
	
	function button(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
		button::$dbcon = $db;
	}
	
	function edit_button($buttonId,$button_html_code){
		button::$dbcon->query("UPDATE `".constant('DBNAME')."`.`button` SET `text`='".str_ireplace("'","''", stripslashes($button_html_code))."' WHERE `Id` ='".$buttonId."' ");
	}
	
	function create_button($button_name,$button_items,$button_group) {
		$button_text = '';
		if($this->check_button_availibility($button_name)){	
			if(empty($button_items)){       $button_text .= $this->build_single_button($button_name); }
			elseif (!empty($button_items)){ $button_text .= $this->build_multiple_button($button_name,$button_items); }
			$button_text = addslashes($button_text);
			button::$dbcon->query("INSERT INTO `".constant('DBNAME')."`.`menu_buttons` (`root_item`,`button_text`,`StatusID`) VALUES ('".$button_name."','".$button_text."','".constant('ENABLED')."')");
			$BtnID = button::$dbcon->insert_id;
			$get_group_id = button::$dbcon->get_row("SELECT GroupId FROM `group` WHERE `Group` ='".$button_group."' ");
			button::$dbcon->query("INSERT INTO `".constant('DBNAME')."`.group_buttons (`GroupId`,`Menu_ButtonId`) VALUES ('".$get_group_id->GroupId."','".$BtnID."') ");
		}
	}
	
	function check_button_availibility($button_name){
    $check_avaliability = button::$dbcon->get_row("SELECT * FROM `menu_buttons` WHERE `root_item` ='".$button_name."' ");
    if(    empty($check_avaliability)) { $available = true;  }
    elseif(!empty($check_avaliability)){ $available = false; }
    return $available;
	}
	
	function build_single_button($button_name){
	   return "<td valign=\"top\"><button class=\"btn\" onclick=\"load_page('".$button_name.".php');\">".$button_name."</button><br /></td>";
	}
	
	function build_multiple_button($button_name,$button_items){
  	$button_text = "<td valign=\"top\">";
    $button_text .= "<button class=\"btn\" onmouseover=\"show('".$button_name."');\" onmouseout=\"hide('".$button_name."');\">".$button_name."</button>";
		$button_text .= "<br />";
		$button_text .= "<div id=\"$button_name\" style=\"display:none; position:absolute; top:15px;\" onmouseover=\"show('".$button_name."');\" onmouseout=\"hide('".$button_name."')\" >";
		$button_text .= "<div>&nbsp;</div>";
		try{
			$items = array();
			$items = explode(",",$button_items);
			foreach ($items as $sub_button){
        if($button_name == $sub_button){
				  print "Dont add sub items with the same name as the root item.";
				  break;
				}
				$button_text .= "<button class=\"btn\" onclick=\"load_page('".$sub_button.".php');\" >".$sub_button."</button>";
		  }
		  $button_text .= "</div>";
		}catch (Exception $e){
      print "Error : ".$e;
    }
    $button_text .= "</td>";
    return $button_text;
	}
	
	function delete_button($buttonId){
		button::$dbcon->query("DELETE FROM `group_buttons` WHERE `group_buttons`.`ButtonId` = ".$buttonId);
		button::$dbcon->query("DELETE FROM `button` WHERE `button`.`Id` = ".$buttonId);
	}
	
	function listall_buttons($limit){
		$buttons = button::$dbcon->get_results("SELECT * FROM `button` ");
		return $buttons;
	}
	
	function listall_buttons_db($db,$limit){
		$buttons = $db->get_results("SELECT * FROM `button` ");
		return $buttons;
	}
	
	function toggle_button_status($buttonId,$statusId){
		button::$dbcon->query("UPDATE `button` SET `StatusID` = '".$statusId."' WHERE `Id` = '".$buttonId."' ");
		return true;
	}
	
	function get_user_buttons($session_gid){
		$allowed_buttons = button::$dbcon->get_results("SELECT `text` FROM `button` WHERE `Id` IN ( SELECT Id FROM group_buttons WHERE GroupId = '".$session_gid."' AND StatusID = '".constant('ENABLED')."' )");
		return $allowed_buttons;
	}
	
	
	// Header Buttons
	
	function create_menu_html(){ // When editing the MENU, update SQL, and write to a file when wanting to access the MENU, then just read the file. much less database load issues
		$root_buttons = $this->group_root_buttons($_SESSION['user_type_id']);
		$button_html = $this->create_root_children($root_buttons);
		return $button_html;
	}
	
	function group_root_buttons($gid){
		$buttons = button::$dbcon->get_results(" SELECT * FROM `button` JOIN `group_buttons` ON (`button`.`Id` = `group_buttons`.`ButtonId`)
																				   WHERE `button`.`ChildButtonId` is null AND `group_buttons`.`GroupId` = '".$gid."' AND StatusID = '".constant('ENABLED')."' ");
		return $buttons;
	}
	
	function create_root_children($root_buttons){
		
		$button_html = '';
		if(is_array($root_buttons)){
				foreach($root_buttons as $root_button){
						$button_html .= $this->create_button_html($root_button,$this->listall_root_children($root_button->Id));
				}
		}else{
				$button_html .= $this->create_button_html($this->listall_root_children($root_buttons->Id));
		}
		return $button_html;
	}
	
	function listall_root_children($rootid){
		$root_children = button::$dbcon->get_results("SELECT `button`.*,`page`.`PageName` FROM `button` JOIN `page` ON (`button`.`PageId` = `page`.`PageId`) WHERE `button`.`ChildButtonId` = '".$rootid."'  ");
		return $root_children;
	}
	
	function create_button_html($root_button,$buttons_db){
		$children_buttons_html='';
		if(is_array($buttons_db)){
				foreach($buttons_db as $button_db){
						$children_buttons_html .= "<button class=\"btn\" onclick=\"load_page('".$button_db->PageName."');\" >".$button_db->text."</button><br />";
				}
		}else{
				$children_buttons_html .= "<button class=\"btn\" onclick=\"load_page('".$buttons_db->PageName."');\" >".$buttons_db->text."</button><br />";
		}
		$root_button_html = "<button class=\"btn\" onmouseover=\"show_menu('".$root_button->text."');\" onmouseout=\"hide_menu('".$root_button->text."');\">".$root_button->text."</button>";
		$root_children_div_html = "<div id=\"".$root_button->text."\" style=\"display:none; position:absolute; top:15px;\" onmouseover=\"show_menu('".$root_button->text."');\" onmouseout=\"hide_menu('".$root_button->text."');\">".
		                          "<div>&nbsp;</div>".$children_buttons_html.
															"</div>";
		return "<td valign=\"top\">".$root_button_html."<br/>".$root_children_div_html."</td>";
	}
	
	
	
	
	
}









?>