<?php
check_dependancy("ez_sql_core.php");

class cdbcon extends ezSQLcore {
	static $dbcon;
	private $username;
	private $password;
	private $dbname;
	private $host;
	
	function cdbcon(){
		// create a constant, so that you can choose the driver in the constants
		$this->dbconect(constant('DBUSER'),constant('DBPASS'),constant('DBNAME'),constant('DBHOST'));
	}
	
	function dbconect($username,$password,$dbname,$host){
		try{
			// MYSQL:
			$db = new ezSQL_mysql($username,$password,$dbname,$host);
			
			// POSTGRES: 
			//$db = new ezSQL_postgresql($username,$password,$dbname,$host);
			
			//$db->use_disk_cache = true;
      //$db->cache_queries = true;
      //$db->cache_timeout = 24;
			
			$this->is_connected($db);
		}catch (Exception $e){
			// handle this 
		}
		cdbcon::$dbcon = $db;
	}
	
	function is_connected($db){
		if (is_object($db)){ $_SESSION['dbconnected'] = true;
		}else{               $_SESSION['dbconnected'] = false; }
	}
	
	function return_db_con(){
		return cdbcon::$dbcon;
	}
}


?>