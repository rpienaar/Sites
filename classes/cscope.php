<?php
check_dependancy('render.php');

class cscope {
	static $dbcon;
	
	function cscope(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
		cscope::$dbcon = $db;
	}
	
	function listall_scopes(){
		$scopes = cscope::$dbcon->get_results(" SELECT * FROM `scope` ");
		return $scopes;
	}
	
	function scope_html_select(){
		$scopes = $this->listall_scopes();
		if(is_array($scopes)){
			$scope_html_options = '';	
			foreach($scopes as $s){
				$scope_html_options .= render::ce("option","value='".$s->Id."'",$s->Scope);
			}
			$scope_html_options = render::ce("select","id='scope_select'|name='scope_select'",$scope_html_options);
		}
		return $scope_html_options;
	}
	
	function validate_scope($scope_id){
		$reseponse = cfile::$dbcon->get_var(" SELECT count(*) FROM `scope` WHERE `Id` = '".$scope_id."' ");
		if($reseponse != 0){
			return true;
		}else{
			return false;
		}
	}
	
}

?>