<?php
check_dependancy("common.php");

class pages extends common {
	static $dbcon;

	function pages(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
  	pages::$dbcon = $db;
	}

	function add_page_to_db($pagename,$status){
		$page = $this->get_page_id_from_name($pagename);
		if($page){
				return false;
		}else{
				pages::$dbcon->query("INSERT INTO `page` (`PageName`,`StatusID`) VALUES ('".$pagename."','".$status."')");
				return true;
		}
	}
	
	function toggle_page_status($page_id,$status){
		pages::$dbcon->query("UPDATE `page` SET `StatusID` = '".$status."' WHERE `PageId` = '".$page_id."' ");
	}
	
	function delete_page($page_id){
		$this->toggle_page_status($page_id,constant('DELETED'));
	}
	
	function listall_pages($LIMIT){
		$pages = pages::$dbcon->get_results("SELECT * FROM `page` WHERE `StatusID` = '".constant('ENABLED')."' OR `StatusID` = '".constant('DISABLED')."' LIMIT ".$LIMIT);
		return $pages;
	}
	
	function listall_pages_db($db,$LIMIT){
		$pages = $db->get_results("SELECT * FROM `page` WHERE `StatusID` = '".constant('ENABLED')."' OR `StatusID` = '".constant('DISABLED')."' LIMIT ".$LIMIT);
		return $pages;
	}
	
	function get_page_picture($page){
		$PP = pages::$dbcon->get_row("SELECT f.Name,pp.PageId FROM page p JOIN page_picture pp ON ( p.PageId = pp.PageId ) JOIN file f ON ( pp.FileId = f.FileId )  WHERE `PageName` = '".$page."' ");
		return $PP;
	}
	
	function set_page_picture($pageid,$fid){
		$count = pages::$dbcon->get_var(" SELECT count(*) FROM `page_picture` WHERE `PageId` = '".$pageid."' ");
		if($count == 0){
			$SQL = " INSERT INTO `page_picture` (`PageId`,`FileId`) VALUES (".$pageid.",".$fid.") ";
		}else{
			$SQL = " UPDATE `page_picture` SET `FileId` = ".$fid." WHERE `PageId` = ".$pageid;
		}
		print $SQL;
		pages::$dbcon->query($SQL);
	}
	
	function open_group_page($gid,$page){
		$PAGE_SQL = " SELECT page.PageId, page.StatusId FROM group_page JOIN page ON ( group_page.PageId = page.PageId )
  	              WHERE group_page.GroupId = '".$gid."' AND page.PageName = '".$page."' ";
	  $page_details = pages::$dbcon->get_row($PAGE_SQL);
	  return $page_details;
	}
	
	function get_page_id_from_name($PageName){ // 
		$PageId = pages::$dbcon->get_var("SELECT `PageId` FROM `page` WHERE `PageName` = '".$PageName."' ");
		return($PageId);
	}
	
	function get_page_name_by_pageid($pageid){
		$lastpage = pages::$dbcon->get_var("SELECT `PageName` FROM `page` WHERE `PageId` = '".$pageid."'");
		return $lastpage;
	}
	
}
?>