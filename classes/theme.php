<?php
check_dependancy("common.php");

class theme extends common {
	private $themeid;
	private $theme;
	private $file_id;
	private $fid;
  private $fname;
  private $cssref;
  private $opacity;
  private $theme_color_settings;
  private $location;
  static $dbcon;
  
  function theme(){
  	$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
  	theme::$dbcon = $db;
  }

  function create_theme($location,$theme,$theme_color_settings){
    $elements = file_get_contents(constant('CSS')."default.css");
    $col_arr = array();
    $col_arr = explode("|",$theme_color_settings);
    
    $filtered_elem1  = str_ireplace("COLOR_1",$col_arr[0].";",$elements);
    $filtered_elem2  = str_ireplace("COLOR_2",$col_arr[1].";",$filtered_elem1);
    $filtered_elem3  = str_ireplace("COLOR_3",$col_arr[2].";",$filtered_elem2);
    $filtered_elem4  = str_ireplace("COLOR_4",$col_arr[3].";",$filtered_elem3);
    $filtered_elem5  = str_ireplace("COLOR_5",$col_arr[4].";",$filtered_elem4);
    $filtered_elem6  = str_ireplace("COLOR_6",$col_arr[5].";",$filtered_elem5);
    $filtered_elem7  = str_ireplace("COLOR_7",$col_arr[6].";",$filtered_elem6);
    $filtered_elem8  = str_ireplace("COLOR_8",$col_arr[7].";",$filtered_elem7);
    $filtered_elem9  = str_ireplace("COLOR_9",$col_arr[8].";",$filtered_elem8);
    $filtered_elem10 = str_ireplace("COLOR_a",$col_arr[9].";",$filtered_elem9);
    $filtered_elem11 = str_ireplace("COLOR_b",$col_arr[10].";",$filtered_elem10);
    $filtered_elem12 = str_ireplace("COLOR_c",$col_arr[11].";",$filtered_elem11);
    
		$fh = fopen(constant('ACTIVE_PATH')."theme_css/".$theme.".css",'w') or print("can't open file");
		fwrite($fh,$filtered_elem12);
		fclose($fh);
		
		$cssrow = theme::$dbcon->get_row("SELECT * FROM `theme` WHERE `cssRef` = '".$theme.".css' ");
		if(empty($cssrow)){
			theme::$dbcon->query("INSERT INTO `theme` (`Theme`,`cssRef`) VALUES ('".$theme."','".$theme.".css')");
    	$themeId = theme::$dbcon->insert_id;
    	theme::$dbcon->query("INSERT INTO `theme_details` VALUES ('".$themeId."',
    					 		'".$col_arr[0]."','".$col_arr[1]."','".$col_arr[2]."','".$col_arr[3]."','".$col_arr[4]."','".$col_arr[5]."',
               		'".$col_arr[6]."','".$col_arr[7]."','".$col_arr[8]."','".$col_arr[9]."','".$col_arr[10]."','".$col_arr[11]."')");
		}else{
			theme::$dbcon->query("UPDATE `theme_details` SET `detail_1` = '".$col_arr[0]."',`detail_2` = '".$col_arr[1]."',`detail_3` = '".$col_arr[2]."',
																						           `detail_4` = '".$col_arr[3]."',`detail_5` = '".$col_arr[4]."',`detail_6` = '".$col_arr[5]."',
																						           `detail_7` = '".$col_arr[6]."',`detail_8` = '".$col_arr[7]."',`detail_9` = '".$col_arr[8]."',
																						           `detail_10` = '".$col_arr[9]."',`detail_11` = '".$col_arr[10]."',`detail_12` = '".$col_arr[11]."'
																			 WHERE `ThemeId` = '".$cssrow->ThemeId."' ");
		}
  }
  
  function delete_theme($location,$theme_name){
    theme::$dbcon->query("DELETE FROM `theme` WHERE `cssRef` = '".$theme_name."' limit 1");
    @unlink($location."css/".$theme_name);
  }
	
	function get_user_customization() {
    if(isset($_SESSION['fname']) && isset($_SESSION['FID']) && isset($_SESSION['opacity']) && isset($_SESSION['theme']) && isset($_SESSION['trans_buttons']) ){
      return $this->get_custom_session_data();
    }elseif (isset($_SESSION['ui'])){
      $SQL = "
       SELECT ud.UserDetailsId, ud.BackgroundId, ud.UseBackground, ud.ThemeId, ud.Opacity, ud.TrsButtons, f.FileId, f.Name, t.cssRef
       FROM userdetails ud
       JOIN background b ON ( ud.BackgroundId = b.BackgroundId ) 
       JOIN file f       ON ( b.fileId        = f.fileId )
       JOIN theme t      ON ( ud.ThemeId      = t.ThemeId )
       WHERE ud.UserDetailsId = '".$_SESSION['uid']."' 
       ";
			$custom = theme::$dbcon->get_row($SQL);
			$av = theme::$dbcon->get_row("SELECT f.Name FROM userdetails ud JOIN file f ON ( ud.AvatarId = f.fileId ) WHERE ud.UserDetailsId = '".$_SESSION['uid']."' ");			
      if($custom->UseBackground == "1"){
        return $this->set_custom_session_data($custom->FileId,$custom->Name,$custom->cssRef,$custom->Opacity,$custom->TrsButtons,$av->Name);
      }elseif($custom->UseBackground == "0"){
        return $this->set_custom_session_data(NULL,NULL,$custom->cssRef,$custom->Opacity,$custom->TrsButtons,$av->Name);
      }
    }
  }
  
  function view_theme($themeid){
  	$theme = theme::$dbcon->get_row("SELECT * FROM `theme` WHERE `ThemeId` = ".$themeid);
  	return $theme;
  }
  
  function listall_themes($limit){
  	$themes = theme::$dbcon->get_results("SELECT * FROM `theme` LIMIT ".$limit);
  	return $themes;
  }
  
  function set_session_data($fid,$fname,$cssref,$opacity,$TransparentMenuButtons,$av_file_name){
    $_SESSION['FID']           = $fid;
    $_SESSION['fname']         = $fname;
    $_SESSION['theme']         = $cssref;
    $_SESSION['opacity']       = $opacity;
    $_SESSION['trans_buttons'] = $TransparentMenuButtons;
    $_SESSION['av_file_name']  = $av_file_name;
  }
  
  function set_custom_session_data($fid,$fname,$cssref,$opacity,$TransparentMenuButtons,$av_file_name){
    $_SESSION['FID']           = $fid;
    $_SESSION['fname']         = $fname;
    $_SESSION['theme']         = $cssref;
    $_SESSION['opacity']       = $opacity;
    $_SESSION['trans_buttons'] = $TransparentMenuButtons;
    $_SESSION['av_file_name']  = $av_file_name;
    $custom_user_data = array("FID"           => $fid,
                              "fname"         => $fname,
                              "theme"         => $cssref,
                              "opacity"       => $opacity,
                              "trans_buttons" => $TransparentMenuButtons,
                              "av_file_name"  => $av_file_name
                              );
    return $custom_user_data;
  }
  
  function get_custom_session_data(){
    $data = array("FID"           => $_SESSION['FID'],
                  "fname"         => $_SESSION['fname'],
                  "theme"         => $_SESSION['theme'],
                  "opacity"       => $_SESSION['opacity'],
                  "trans_buttons" => $_SESSION['trans_buttons'],
                  "av_file_name"  => $_SESSION['av_file_name']
                 );
    return $data;
  }
  
  function theme_id_from_name($themeName){
  	$res = theme::$dbcon->get_row("SELECT `ThemeId` FROM `theme` WHERE `Theme` = '".$themeName."' ");
		return $themeid = $res->ThemeId;
  }
  
  function open_theme_detail($themeid){
		$theme_details = theme::$dbcon->get_row("SELECT * FROM `theme_details` WHERE `ThemeId` = '".$themeid."' ");
		return $theme_details;
  }
	
}
$user_custom = new theme();
/*
if($_POST){
  if(!empty($_POST['theme_name'])){
    $valid_counter = 0;
    $nmr_of_colour_settings = 12;
    $color_settings = array();
    for($a = 1;$a <= $nmr_of_colour_settings;$a++){
      if($_POST['a_t_'.$a] > ''){
        $valid_counter++;
        $color_settings[$a] = $_POST['a_t_'.$a];
      }else{
        break;
      }
    }
    if($valid_counter = $nmr_of_colour_settings){ 
      $user_custom->create_theme($location,strip_tags($_POST['theme_name']),implode("|",$color_settings));
    }
  }elseif(!empty($_POST['del_theme'])){
    $user_custom->delete_theme($location,strip_tags($_POST['del_theme']));
  }
}
*/

$CustomUserDetails = array();
$CustomUserDetails = $user_custom->get_user_customization();
$fname         = $CustomUserDetails['fname'];
$FID           = $CustomUserDetails['FID'];
$opacity       = $CustomUserDetails['opacity'];
$theme         = $CustomUserDetails['theme'];
$trans_buttons = $CustomUserDetails['trans_buttons'];
$av_file_name  = $CustomUserDetails['av_file_name'];

?>