-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 19, 2012 at 07:58 AM
-- Server version: 5.1.52
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `k37577_kodingen`
--

-- --------------------------------------------------------

--
-- Table structure for table `background`
--

CREATE TABLE IF NOT EXISTS `background` (
  `BackgroundId` int(9) NOT NULL AUTO_INCREMENT,
  `FileId` int(9) NOT NULL,
  PRIMARY KEY (`BackgroundId`),
  KEY `fileid` (`FileId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `background`
--

INSERT INTO `background` (`BackgroundId`, `FileId`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `calendar_activities`
--

CREATE TABLE IF NOT EXISTS `calendar_activities` (
  `ActivityId` int(9) NOT NULL AUTO_INCREMENT,
  `Activity` text NOT NULL,
  `Status` varchar(255) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserId` int(9) NOT NULL,
  PRIMARY KEY (`ActivityId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `calendar_activities`
--

INSERT INTO `calendar_activities` (`ActivityId`, `Activity`, `Status`, `Timestamp`, `UserId`) VALUES
(2, 'l', '0', '2012-01-13 06:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `ChatId` int(9) NOT NULL AUTO_INCREMENT,
  `UserId` int(9) NOT NULL,
  `Online` int(1) NOT NULL,
  PRIMARY KEY (`ChatId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `chat_message`
--

CREATE TABLE IF NOT EXISTS `chat_message` (
  `ChatMessageId` int(9) NOT NULL AUTO_INCREMENT,
  `ChatId` int(9) NOT NULL,
  `Message` text NOT NULL,
  PRIMARY KEY (`ChatMessageId`),
  KEY `chatid` (`ChatId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `FileId` int(9) NOT NULL AUTO_INCREMENT,
  `Name` varchar(244) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `UploadDate` date NOT NULL,
  `ScopeId` int(9) NOT NULL,
  PRIMARY KEY (`FileId`),
  KEY `ScopeId` (`ScopeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`FileId`, `Name`, `Type`, `UploadDate`, `ScopeId`) VALUES
(1, 'background.jpg', 'image/jpeg', '2008-11-16', 3),
(2, 'DefaultAvatar.png', 'image/png', '2010-06-21', 3),
(3, 'admin_banner.gif', 'image/gif', '2010-01-01', 3),
(4, 'home_banner.gif', 'image/gif', '2010-01-01', 3),
(5, 'content_banner.gif', 'image/gif', '2010-01-01', 3),
(6, 'system_settings.gif', 'image/gif', '2010-01-01', 3),
(7, 'personalize_banner.gif', 'image/gif', '2010-01-01', 3),
(8, 'file_upload_icon.jpg', 'image/jpeg', '2010-01-01', 3),
(11, '7d4224d28aa8c852a3c74f2363191d9489bb88e5_m.jpg', 'image/jpeg', '2012-01-20', 2),
(12, '303699_102011509909616_100003023019816_15074_248594171_n.jpeg', 'image/jpeg', '2012-01-20', 2),
(13, 'hscirocco.png', 'image/png', '2012-01-20', 2),
(14, '20500825.jpg', 'image/jpeg', '2012-01-20', 2),
(15, '50-Hottest-Underboob-Photos-of-All-Time.jpg', 'image/jpeg', '2012-01-21', 2),
(16, '50-Hottest-Underboob-Photos-of-All-Time.jpg', 'image/jpeg', '2012-01-21', 2),
(17, '194709_10151067030525717_803520716_22124853_540080532_o.jpg', 'image/jpeg', '2012-01-21', 2),
(18, '7d5350a845040df09b8c5a3ef2e25227.jpg', 'image/jpeg', '2012-01-21', 2),
(19, '165.jpg', 'image/jpeg', '2012-01-21', 2),
(21, 'funny-awesome-photos-thechive-192.jpg', 'image/jpeg', '2012-01-24', 2),
(22, '225225_10150191351957983_761487982_7226594_8377708_n.jpg', 'image/jpeg', '2012-01-24', 2),
(23, '456748.jpg', 'image/jpeg', '2012-01-24', 2),
(24, '20500825.jpg', 'image/jpeg', '2012-01-24', 2),
(25, 'a-apple-deathstar (1).jpg', 'image/jpeg', '2012-01-24', 2),
(26, '382651_10150537342280809_125281815808_10704706_1682006083_n.jpeg', 'image/jpeg', '2012-01-24', 2),
(27, '2655188094_ae0f451fb8_o.png', 'image/png', '2012-01-24', 2),
(28, 'af3e056507ff639f0f6fe2519318a01a.jpg', 'image/jpeg', '2012-01-24', 2),
(29, '399703_327424280623591_187391387960215_1078892_2047200236_n.jpeg', 'image/jpeg', '2012-01-24', 2),
(30, '4943765350_9ec51b5e61_z.jpg', 'image/jpeg', '2012-01-24', 2),
(31, '129410994356883603.jpg', 'image/jpeg', '2012-01-24', 2),
(32, '303699_102011509909616_100003023019816_15074_248594171_n.jpeg', 'image/jpeg', '2012-01-24', 2),
(33, '6a00d83452cd1869e20147e2d6c5b5970b-800wi.jpg', 'image/jpeg', '2012-01-24', 1),
(34, '019.jpg', 'image/jpeg', '2012-01-24', 1),
(35, '225225_10150191351957983_761487982_7226594_8377708_n.jpg', 'image/jpeg', '2012-01-24', 1),
(36, '456748.jpg', 'image/jpeg', '2012-01-24', 1),
(37, '2655188094_ae0f451fb8_o.png', 'image/png', '2012-01-24', 1),
(38, '4504_84801517982_761487982_1973957_5047232_n.jpg', 'image/jpeg', '2012-01-24', 1),
(39, '50-Hottest-Underboob-Photos-of-All-Time.jpg', 'image/jpeg', '2012-01-24', 1),
(40, '320750_108068329303934_100003023019816_60455_1299753811_n.jpeg', 'image/jpeg', '2012-01-24', 2),
(41, 'Pretoria.png', 'image/x-png', '2012-01-24', 2),
(42, 'Chrysanthemum.jpg', 'image/pjpeg', '2012-01-24', 1),
(43, 'angelarzrv_aug09.jpg', 'image/jpeg', '2012-01-25', 1),
(44, 'girls-havda-moe-920-1.jpg', 'image/jpeg', '2012-01-25', 1),
(45, 'girls-havda-moe-920-2.jpg', 'image/jpeg', '2012-01-25', 1),
(46, 'girls-havda-moe-920-3.jpg', 'image/jpeg', '2012-01-25', 1),
(47, 'girls-havda-moe-920-4.jpg', 'image/jpeg', '2012-01-25', 1),
(48, 'girls-havda-moe-920-5.jpg', 'image/jpeg', '2012-01-25', 1),
(49, 'girls-havda-moe-920-6.jpg', 'image/jpeg', '2012-01-25', 1),
(50, 'girls-havda-moe-920-7.jpg', 'image/jpeg', '2012-01-25', 1),
(51, 'girls-havda-moe-920-8.jpg', 'image/jpeg', '2012-01-25', 1),
(52, 'girls-havda-moe-920-9.jpg', 'image/jpeg', '2012-01-25', 1),
(53, 'girls-havda-moe-920-10.jpg', 'image/jpeg', '2012-01-25', 1),
(54, 'girls-havda-moe-920-11.jpg', 'image/jpeg', '2012-01-25', 1),
(55, 'girls-havda-moe-920-12.jpg', 'image/jpeg', '2012-01-25', 1),
(56, 'girls-havda-moe-920-13.jpg', 'image/jpeg', '2012-01-25', 1),
(57, 'girls-havda-moe-920-14.jpg', 'image/jpeg', '2012-01-25', 1),
(58, 'girls-havda-moe-920-15.jpg', 'image/jpeg', '2012-01-25', 1),
(59, 'girl_motorcycle.jpg', 'image/jpeg', '2012-01-25', 1),
(60, '418810_10150713459630809_125281815808_11314491_1661424286_n.jpg', 'image/jpeg', '2012-02-19', 1),
(61, 'a-my-childhood-29.jpg', 'image/jpeg', '2012-02-19', 1),
(62, 'dating-fails-dating-fails-i-follow-you-home-because-i-care.jpg', 'image/jpeg', '2012-02-19', 1),
(63, '6a0120a85dcdae970b0167625041db970b-800wi.png', 'image/png', '2012-02-19', 1),
(64, 'party-fails-fun-for-the-whole-family-disney-on-beer.jpg', 'image/jpeg', '2012-02-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `GroupId` int(9) NOT NULL AUTO_INCREMENT,
  `Group` varchar(50) NOT NULL,
  PRIMARY KEY (`GroupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`GroupId`, `Group`) VALUES
(1, 'Administrator'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `group_buttons`
--

CREATE TABLE IF NOT EXISTS `group_buttons` (
  `GroupButtonId` int(9) NOT NULL AUTO_INCREMENT,
  `GroupId` int(9) NOT NULL,
  `Menu_ButtonId` int(9) NOT NULL,
  PRIMARY KEY (`GroupButtonId`),
  KEY `menubuttonid` (`Menu_ButtonId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `group_buttons`
--

INSERT INTO `group_buttons` (`GroupButtonId`, `GroupId`, `Menu_ButtonId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 2),
(5, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `group_page`
--

CREATE TABLE IF NOT EXISTS `group_page` (
  `GroupPageId` int(9) NOT NULL AUTO_INCREMENT,
  `GroupId` int(9) NOT NULL,
  `PageId` int(9) NOT NULL,
  PRIMARY KEY (`GroupPageId`),
  KEY `groupid` (`GroupId`),
  KEY `userid` (`PageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `group_page`
--

INSERT INTO `group_page` (`GroupPageId`, `GroupId`, `PageId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(20, 2, 9),
(21, 2, 8),
(22, 2, 10),
(23, 2, 11),
(27, 2, 15),
(29, 2, 17),
(30, 2, 18),
(31, 2, 19);

-- --------------------------------------------------------

--
-- Table structure for table `menu_buttons`
--

CREATE TABLE IF NOT EXISTS `menu_buttons` (
  `Menu_ButtonId` int(9) NOT NULL AUTO_INCREMENT,
  `root_item` varchar(255) NOT NULL,
  `StatusID` int(9) NOT NULL,
  `button_text` text NOT NULL,
  PRIMARY KEY (`Menu_ButtonId`),
  KEY `btnstatusid` (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_buttons`
--

INSERT INTO `menu_buttons` (`Menu_ButtonId`, `root_item`, `StatusID`, `button_text`) VALUES
(1, 'admin', 1, '<td valign="top"><button class="btn" onmouseover="show(''admin'');" onmouseout="hide(''admin'');">Admin</button><br />\n                  <div id="admin" style="display:none; position:absolute; top:15px;" onmouseover="show(''admin'');" onmouseout="hide(''admin'');"><div>&nbsp;</div>\n                  <button class="btn" onclick="load_page(''admin_groups.php'');" >        Groups</button><br />\n                  <button class="btn" onclick="load_page(''admin_buttons.php'');" >       Buttons</button><br />\n                  <button class="btn" onclick="load_page(''admin_users.php'');" >         Users</button><br />\n                  <button class="btn" onclick="load_page(''admin_pages.php'');" >         Pages</button><br />\n                  <button class="btn" onclick="load_page(''admin_themes.php'');" >        Themes</button><br />\n                  <button class="btn" onclick="load_page(''admin_rss.php'');" >           Rss</button><br />\n                  <button class="btn" onclick="load_page(''admin_files.php'');" >         Files</button><br />\n                  <button class="btn" onclick="load_page(''planning.php'');">             planning</button><br />\n                  </div></td>'),
(2, 'content', 1, '<td valign="top"><button class="btn" onmouseover="show(''content'');" onmouseout="hide(''content'');">content</button><br />\r\n									<div id="content" style="display:none; position:absolute; top:15px;" onmouseover="show(''content'');" onmouseout="hide(''content'')" ><div>&nbsp;</div>\r\n									<button class="btn" onclick="load_page(''calendar.php''); " >      calendar</button><br />\r\n									<button class="btn" onclick="load_page(''links.php''); " >         links</button><br />\r\n									<button class="btn" onclick="load_page(''videos.php''); " >        Videos</button><br />\r\n									<button class="btn" onclick="load_page(''music.php''); " >         Music</button><br />\r\n									<button class="btn" onclick="load_page(''rss.php''); " >           rss</button><br/>\r\n									<button class="btn" onclick="load_page(''upload.php'');">          upload</button>\r\n									</div></td>'),
(3, 'settings', 1, '<td valign="top"><button class="btn" onmouseover="show(''settings'');" onmouseout="hide(''settings'');">settings</button><br />\n                  <div id="settings" style="display:none; position:absolute; top:15px;" onmouseover="show(''settings'');" onmouseout="hide(''settings'')" ><div>&nbsp;</div>\n                  <button class="btn" onclick="load_page(''personalize.php'');">personalize</button><br />\n                  <button class="btn" onclick="load_page(''my_settings.php'');">my_settings</button>\n                  </div></td>');

-- --------------------------------------------------------

--
-- Table structure for table `music_player`
--

CREATE TABLE IF NOT EXISTS `music_player` (
  `music_player_id` int(9) NOT NULL AUTO_INCREMENT,
  `UserDetailsId` int(9) NOT NULL,
  `alphabetize` enum('0','1') NOT NULL DEFAULT '0',
  `autoload` enum('0','1') NOT NULL DEFAULT '0',
  `autoplay` enum('0','1') NOT NULL DEFAULT '0',
  `repeat` enum('0','1') NOT NULL DEFAULT '0',
  `repeat_playlist` enum('0','1') NOT NULL DEFAULT '0',
  `shuffle` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`music_player_id`),
  KEY `musicuserdetailsid` (`UserDetailsId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `music_player`
--

INSERT INTO `music_player` (`music_player_id`, `UserDetailsId`, `alphabetize`, `autoload`, `autoplay`, `repeat`, `repeat_playlist`, `shuffle`) VALUES
(1, 1, '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `org_keyword`
--

CREATE TABLE IF NOT EXISTS `org_keyword` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `Topic_id` int(9) NOT NULL,
  `Keyword` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `kywordtopicid` (`Topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `org_topic`
--

CREATE TABLE IF NOT EXISTS `org_topic` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `ParentTopicId` int(9) DEFAULT NULL,
  `UserId` int(9) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Body` text,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `userid` (`UserId`),
  KEY `parenttopicid` (`ParentTopicId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `PageId` int(9) NOT NULL AUTO_INCREMENT,
  `PageName` varchar(255) NOT NULL,
  `StatusID` int(9) NOT NULL,
  PRIMARY KEY (`PageId`),
  UNIQUE KEY `PageName` (`PageName`),
  KEY `pagestatusid` (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`PageId`, `PageName`, `StatusID`) VALUES
(1, 'admin_buttons.php', 1),
(2, 'admin_groups.php', 1),
(3, 'admin_pages.php', 1),
(4, 'admin_users.php', 1),
(5, 'admin_themes.php', 1),
(6, 'admin_rss.php', 1),
(7, 'admin_files.php', 1),
(8, 'calendar.php', 1),
(9, 'home.php', 1),
(10, 'links.php', 1),
(11, 'personalize.php', 1),
(12, 'planning.php', 1),
(13, 'upload.php', 1),
(14, 'videos.php', 1),
(15, 'organiser.php', 1),
(16, 'music.php', 1),
(17, 'rss.php', 1),
(18, 'app.php', 1),
(19, 'my_settings.php', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_picture`
--

CREATE TABLE IF NOT EXISTS `page_picture` (
  `PagePictureId` int(9) NOT NULL AUTO_INCREMENT,
  `PageId` int(9) NOT NULL,
  `FileId` int(9) NOT NULL,
  PRIMARY KEY (`PagePictureId`),
  KEY `PageId` (`PageId`),
  KEY `FileId` (`FileId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `page_picture`
--

INSERT INTO `page_picture` (`PagePictureId`, `PageId`, `FileId`) VALUES
(1, 1, 3),
(2, 2, 3),
(3, 3, 3),
(4, 4, 3),
(5, 5, 3),
(6, 6, 3),
(7, 7, 3),
(8, 8, 5),
(9, 9, 4),
(10, 10, 5),
(11, 11, 7),
(12, 12, 3),
(13, 13, 5),
(14, 14, 5),
(15, 15, 5),
(16, 16, 5),
(17, 17, 5),
(18, 18, 1),
(19, 19, 6);

-- --------------------------------------------------------

--
-- Table structure for table `planning`
--

CREATE TABLE IF NOT EXISTS `planning` (
  `PlanningId` int(9) NOT NULL AUTO_INCREMENT,
  `Description` text NOT NULL,
  `Status` varchar(20) NOT NULL DEFAULT 'False',
  `UserId` int(9) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PlanningId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE IF NOT EXISTS `playlist` (
  `PlaylistId` int(9) NOT NULL AUTO_INCREMENT,
  `PlaylistURL` text NOT NULL,
  PRIMARY KEY (`PlaylistId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`PlaylistId`, `PlaylistURL`) VALUES
(1, 'playlist.xspf'),
(2, 'playlist.xspf'),
(3, 'playlist.xspf'),
(4, 'playlist.xspf'),
(5, 'playlist.xspf');

-- --------------------------------------------------------

--
-- Table structure for table `posted_links`
--

CREATE TABLE IF NOT EXISTS `posted_links` (
  `linkId` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `UserId` int(9) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`linkId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rss_feed`
--

CREATE TABLE IF NOT EXISTS `rss_feed` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `URL` text NOT NULL,
  `ScopeId` int(9) NOT NULL,
  `last_refresh` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `feed_type` varchar(255) DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `ScopeId` (`ScopeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `rss_feed`
--

INSERT INTO `rss_feed` (`Id`, `URL`, `ScopeId`, `last_refresh`, `feed_type`) VALUES
(1, 'http://henrik.nyh.se/scrapers/cyanide_and_happiness.rss', 1, '2012-01-20 11:57:37', '2');

-- --------------------------------------------------------

--
-- Table structure for table `rss_feed_entry`
--

CREATE TABLE IF NOT EXISTS `rss_feed_entry` (
  `EntryId` int(9) NOT NULL AUTO_INCREMENT,
  `FeedId` int(9) NOT NULL,
  `channel` text,
  `title` text,
  `link` text,
  `comments` text,
  `pubDate` text,
  `timestamp` text,
  `description` text,
  `isPermaLink` text,
  `creator` text,
  `content` text,
  `commentRss` text,
  PRIMARY KEY (`EntryId`),
  KEY `FeedId` (`FeedId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `rss_feed_entry`
--

INSERT INTO `rss_feed_entry` (`EntryId`, `FeedId`, `channel`, `title`, `link`, `comments`, `pubDate`, `timestamp`, `description`, `isPermaLink`, `creator`, `content`, `commentRss`) VALUES
(1, 1, NULL, '01.20.2012', NULL, NULL, 'Fri, 20 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/drugs.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(2, 1, NULL, 'We''re back!', NULL, NULL, 'Thu, 19 Jan 2012 00:18:34 +0000', NULL, 'News/Article', NULL, NULL, NULL, NULL),
(3, 1, NULL, '01.19.2012', NULL, NULL, 'Thu, 19 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Dave/comicpushpush1.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(4, 1, NULL, '01.18.2012', NULL, NULL, 'Wed, 18 Jan 2012 00:00:00 +0000', NULL, '<img src="http://flashasylum.com/db/files/Comics/Rob/missed-a-day.jpg" alt="Comic" >', NULL, NULL, NULL, NULL),
(5, 1, NULL, '01.17.2012', NULL, NULL, 'Tue, 17 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Matt/He-died-how-he-lived...-drinking.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(6, 1, NULL, '01.16.2012', NULL, NULL, 'Mon, 16 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Kris/peeve.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(7, 1, NULL, '01.15.2012', NULL, NULL, 'Sun, 15 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/homies.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(8, 1, NULL, '01.14.2012', NULL, NULL, 'Sat, 14 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Dave/comictwodadsnew2.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(9, 1, NULL, '01.13.2012', NULL, NULL, 'Fri, 13 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Kris/experiment.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(10, 1, NULL, '01.12.2012', NULL, NULL, 'Thu, 12 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/9months.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(11, 1, NULL, '01.24.2012', NULL, NULL, 'Tue, 24 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Matt/Dress-for-the-job-you-want...-sometimes..png" alt="Comic" >', NULL, NULL, NULL, NULL),
(12, 1, NULL, 'Delicious News', NULL, NULL, 'Mon, 23 Jan 2012 14:53:00 +0000', NULL, 'News/Article', NULL, NULL, NULL, NULL),
(13, 1, NULL, '01.23.2012', NULL, NULL, 'Mon, 23 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/dragon.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(14, 1, NULL, '01.22.2012', NULL, NULL, 'Sun, 22 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Dave/comictwodadshomework1.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(15, 1, NULL, '01.21.2012', NULL, NULL, 'Sat, 21 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Kris/grave.png" alt="Comic" >', NULL, NULL, NULL, NULL),
(16, 1, NULL, 'New signed print', NULL, NULL, 'Fri, 20 Jan 2012 17:36:01 +0000', NULL, 'News/Article', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scope`
--

CREATE TABLE IF NOT EXISTS `scope` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `Scope` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `scope`
--

INSERT INTO `scope` (`Id`, `Scope`) VALUES
(1, 'public'),
(2, 'private'),
(3, 'system'),
(4, 'rss_feed_picture'),
(5, 'SCOPE_FEED_CYANIDE');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `StatusID` int(9) NOT NULL AUTO_INCREMENT,
  `Status` varchar(100) NOT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`StatusID`, `Status`) VALUES
(1, 'Enabled'),
(2, 'Disabled'),
(3, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `ThemeId` int(9) NOT NULL AUTO_INCREMENT,
  `Theme` text NOT NULL,
  `cssRef` varchar(255) NOT NULL,
  PRIMARY KEY (`ThemeId`),
  UNIQUE KEY `cssRef` (`cssRef`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `theme`
--

INSERT INTO `theme` (`ThemeId`, `Theme`, `cssRef`) VALUES
(1, 'elements', 'elements.css'),
(2, 'funky', 'funky.css'),
(3, 'lightblue', 'lightblue.css'),
(4, 'Cool', 'Cool.css'),
(5, 'Black', 'Black.css');

-- --------------------------------------------------------

--
-- Table structure for table `theme_details`
--

CREATE TABLE IF NOT EXISTS `theme_details` (
  `ThemeId` int(9) NOT NULL,
  `detail_1` varchar(20) NOT NULL,
  `detail_2` varchar(20) NOT NULL,
  `detail_3` varchar(20) NOT NULL,
  `detail_4` varchar(20) NOT NULL,
  `detail_5` varchar(20) NOT NULL,
  `detail_6` varchar(20) NOT NULL,
  `detail_7` varchar(20) NOT NULL,
  `detail_8` varchar(20) NOT NULL,
  `detail_9` varchar(20) NOT NULL,
  `detail_10` varchar(20) NOT NULL,
  `detail_11` varchar(20) NOT NULL,
  `detail_12` varchar(20) NOT NULL,
  PRIMARY KEY (`ThemeId`),
  KEY `themeid` (`ThemeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_details`
--

INSERT INTO `theme_details` (`ThemeId`, `detail_1`, `detail_2`, `detail_3`, `detail_4`, `detail_5`, `detail_6`, `detail_7`, `detail_8`, `detail_9`, `detail_10`, `detail_11`, `detail_12`) VALUES
(1, '#FFFFFF', '#000000', '#ababab', '#000000', '#c4c4c4', '#000000', '#FFFFFF', '#000000', '#c4c4c4', '#000000', '#ababab', '#000000'),
(2, '#FFFFFF', '#f48d08', '#d27624', '#000000', '#f8ab48', '#000000', '#d27624', '#000000', '#f8ab48', '#000000', '#d27624', '#000000'),
(3, '#FFFFFF', '#33CCFF', '#3366FF', '#000000', '#33CCFF', '#000000', '#33CCFF', '#000000', '#3366FF', '#000000', '#3366FF', '#000000'),
(4, '#000099', '#3399FF', '#FFCC33', '#000000', '#FF6600', '#000000', '#EEEEEE', '#000000', '#33CCCC', '#000099', '#33CCCC', '#000099'),
(5, '#000000', '#BBBBBB', '#444444', '#EEEEEE', '#000000', '#BBBBBB', '#000000', '#BBBBBB', '#444444', '#EEEEEE', '#000000', '#BBBBBB');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `UserId` int(9) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `GroupId` int(9) NOT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserId`, `Username`, `Password`, `GroupId`) VALUES
(1, 'admin', '6907043ba709491e5c654c4bfe27eab8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE IF NOT EXISTS `userdetails` (
  `UserDetailsId` int(9) NOT NULL AUTO_INCREMENT,
  `Active` varchar(20) NOT NULL,
  `ThemeId` int(9) NOT NULL,
  `BackgroundId` int(9) NOT NULL,
  `UseBackground` varchar(1) NOT NULL,
  `UserId` int(9) NOT NULL,
  `Opacity` int(6) NOT NULL,
  `TrsButtons` varchar(1) NOT NULL,
  `PlaylistId` int(9) NOT NULL,
  `AvatarId` int(9) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `MSISDN` varchar(13) DEFAULT NULL,
  `LastPageId` int(9) DEFAULT NULL,
  `VerificationCode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserDetailsId`),
  KEY `userid` (`UserId`),
  KEY `backgroundid` (`BackgroundId`),
  KEY `playlistid` (`PlaylistId`),
  KEY `AvatarId` (`AvatarId`),
  KEY `LastPageId` (`LastPageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`UserDetailsId`, `Active`, `ThemeId`, `BackgroundId`, `UseBackground`, `UserId`, `Opacity`, `TrsButtons`, `PlaylistId`, `AvatarId`, `Email`, `MSISDN`, `LastPageId`, `VerificationCode`) VALUES
(1, 'true', 5, 1, '0', 1, 100, '0', 1, 41, 'ruan800@gmail.com', NULL, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_rss_feeds`
--

CREATE TABLE IF NOT EXISTS `user_rss_feeds` (
  `UserRssFeedId` int(9) NOT NULL AUTO_INCREMENT,
  `FeedId` int(9) NOT NULL,
  `UserId` int(9) NOT NULL,
  PRIMARY KEY (`UserRssFeedId`),
  KEY `FeedId` (`FeedId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `background`
--
ALTER TABLE `background`
  ADD CONSTRAINT `background_ibfk_1` FOREIGN KEY (`FileId`) REFERENCES `file` (`FileId`);

--
-- Constraints for table `calendar_activities`
--
ALTER TABLE `calendar_activities`
  ADD CONSTRAINT `calendar_activities_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `chat_message`
--
ALTER TABLE `chat_message`
  ADD CONSTRAINT `chat_message_ibfk_1` FOREIGN KEY (`ChatId`) REFERENCES `chat` (`ChatId`);

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`ScopeId`) REFERENCES `scope` (`Id`);

--
-- Constraints for table `group_buttons`
--
ALTER TABLE `group_buttons`
  ADD CONSTRAINT `group_buttons_ibfk_1` FOREIGN KEY (`Menu_ButtonId`) REFERENCES `menu_buttons` (`Menu_ButtonId`);

--
-- Constraints for table `group_page`
--
ALTER TABLE `group_page`
  ADD CONSTRAINT `group_page_ibfk_1` FOREIGN KEY (`GroupId`) REFERENCES `group` (`GroupId`),
  ADD CONSTRAINT `group_page_ibfk_2` FOREIGN KEY (`PageId`) REFERENCES `page` (`PageId`);

--
-- Constraints for table `menu_buttons`
--
ALTER TABLE `menu_buttons`
  ADD CONSTRAINT `menu_buttons_ibfk_1` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

--
-- Constraints for table `org_keyword`
--
ALTER TABLE `org_keyword`
  ADD CONSTRAINT `org_keyword_ibfk_1` FOREIGN KEY (`Topic_id`) REFERENCES `org_topic` (`Id`);

--
-- Constraints for table `org_topic`
--
ALTER TABLE `org_topic`
  ADD CONSTRAINT `org_topic_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`),
  ADD CONSTRAINT `org_topic_ibfk_2` FOREIGN KEY (`ParentTopicId`) REFERENCES `org_topic` (`Id`);

--
-- Constraints for table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `page_ibfk_1` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

--
-- Constraints for table `page_picture`
--
ALTER TABLE `page_picture`
  ADD CONSTRAINT `page_picture_ibfk_1` FOREIGN KEY (`PageId`) REFERENCES `page` (`PageId`),
  ADD CONSTRAINT `page_picture_ibfk_2` FOREIGN KEY (`FileId`) REFERENCES `file` (`FileId`);

--
-- Constraints for table `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `planning_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `posted_links`
--
ALTER TABLE `posted_links`
  ADD CONSTRAINT `posted_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `rss_feed`
--
ALTER TABLE `rss_feed`
  ADD CONSTRAINT `rss_feed_ibfk_1` FOREIGN KEY (`ScopeId`) REFERENCES `scope` (`Id`);

--
-- Constraints for table `rss_feed_entry`
--
ALTER TABLE `rss_feed_entry`
  ADD CONSTRAINT `rss_feed_entry_ibfk_1` FOREIGN KEY (`FeedId`) REFERENCES `rss_feed` (`Id`);

--
-- Constraints for table `theme_details`
--
ALTER TABLE `theme_details`
  ADD CONSTRAINT `theme_details_ibfk_1` FOREIGN KEY (`ThemeId`) REFERENCES `theme` (`ThemeId`);

--
-- Constraints for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD CONSTRAINT `userdetails_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`),
  ADD CONSTRAINT `userdetails_ibfk_2` FOREIGN KEY (`BackgroundId`) REFERENCES `background` (`BackgroundId`),
  ADD CONSTRAINT `userdetails_ibfk_3` FOREIGN KEY (`PlaylistId`) REFERENCES `playlist` (`PlaylistId`),
  ADD CONSTRAINT `userdetails_ibfk_4` FOREIGN KEY (`AvatarId`) REFERENCES `file` (`FileId`),
  ADD CONSTRAINT `userdetails_ibfk_5` FOREIGN KEY (`LastPageId`) REFERENCES `page` (`PageId`);

--
-- Constraints for table `user_rss_feeds`
--
ALTER TABLE `user_rss_feeds`
  ADD CONSTRAINT `user_rss_feeds_ibfk_1` FOREIGN KEY (`FeedId`) REFERENCES `rss_feed` (`Id`),
  ADD CONSTRAINT `user_rss_feeds_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
