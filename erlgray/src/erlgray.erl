%% @author Mochi Media <dev@mochimedia.com>
%% @copyright 2010 Mochi Media <dev@mochimedia.com>

%% @doc erlgray.

-module(erlgray).
-author("Mochi Media <dev@mochimedia.com>").
-export([start/0, stop/0]).

ensure_started(App) ->
    case application:start(App) of
        ok ->
            ok;
        {error, {already_started, App}} ->
            ok
    end.


%% @spec start() -> ok
%% @doc Start the erlgray server.
start() ->
    erlgray_deps:ensure(),
    ensure_started(crypto),
    application:start(erlgray).


%% @spec stop() -> ok
%% @doc Stop the erlgray server.
stop() ->
    application:stop(erlgray).
