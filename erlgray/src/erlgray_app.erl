%% @author Mochi Media <dev@mochimedia.com>
%% @copyright erlgray Mochi Media <dev@mochimedia.com>

%% @doc Callbacks for the erlgray application.

-module(erlgray_app).
-author("Mochi Media <dev@mochimedia.com>").

-behaviour(application).
-export([start/2,stop/1]).


%% @spec start(_Type, _StartArgs) -> ServerRet
%% @doc application start callback for erlgray.
start(_Type, _StartArgs) ->
    erlgray_deps:ensure(),
    erlgray_sup:start_link().

%% @spec stop(_State) -> ServerRet
%% @doc application stop callback for erlgray.
stop(_State) ->
    ok.
