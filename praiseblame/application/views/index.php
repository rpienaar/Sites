<?php
?>
<script type="text/javascript">
  
  function resize_tbl(){
    $('#page_tbl').css('height', $(window).height()+'px');
    setTimeout(function() {
      resize_tbl();
    }, 5000);
  }
  $(document).ready(function() {
    resize_tbl();
  });
</script>

<style>
  table {
    width:100%;
    text-align: center;
    margin: 0px;
    padding: 0px;
    position: absolute;
    top: 0px;
    left: 0px;
    border-collapse: collapse;
  }
  
  table, td, th {
    
  }
  
  table#page_tbl td {
    vertical-align: top;
  }
  
  .neg{
    background-color: #000000;
    color: #FFFFFF; 
  }
  
  .pos{
    background-color: #FFFFFF;
    color: #000000; 
  }
</style>

<table id="page_tbl" >
  <tr>
    <td class="neg" style="height: 50px;">
      <h1>BLAME</h1>  
    </td>
    <td class="pos">
      <h1>PRAISE</h1>
    </td>
  </tr>
  <tr>
    <td class="neg">
      <textarea cols="50" rows="5" id="neg_input"></textarea>
      <br />
      <button>Blame</button>
    </td>
    <td class="pos">
      <textarea cols="50" rows="5" id="pos_input"></textarea>
      <br />
      <button>Praise</button>
    </td>
  </tr>
</table>