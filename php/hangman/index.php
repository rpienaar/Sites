<?php
define('DBUSER','root');
define('DBPASS','root');
define('DBNAME','utils');
define('DBHOST','localhost');
require('class_checker.php');
require('ez_sql_core.php');
require('ez_sql_mysql.php');
require('cdbcon.php');
$dbclass = new cdbcon();
$dbcon = $dbclass->return_db_con();
$max_words = 115832;

function start($dbcon){
  if(isset($_SESSION['start']) && $_SESSION['start'] == 0){
    $randomword=get_random_word($dbcon);
  }
}

function get_random_word(){
  $rndnum = rand(1,115832);
  //$word_row = $dbcon->get_row("SELECT * FROM hangman_words WHERE id = "+$rndnum);
  $name = $word_row->word;
  return $name;
}

function multi_nbsp($times){
  $retrurn_value='';
  for($a=1; $a < $times; $a++){
    $retrurn_value .="&nbsp;";
  }
  return $retrurn_value;
}

function create_guesser(){
  
}

function create_hangman($failed_attempts){
  $var='';
  $times=5;
  $var .= "|----|<br/>";
  
  $var .= "|".multi_nbsp($times);
  if($failed_attempts == 1){$var.="o";}
  $var.="<br/>";
  
  $var .= "|".multi_nbsp($times);
  if($failed_attempts == 2){
    $var.="|";
  }elseif($failed_attempts == 3){
    $var.="-|";
  }elseif($failed_attempts == 4){
    $var.="-|-";
  }
  $var.="<br/>";

  $var .= "|".multi_nbsp($times);
  if($failed_attempts == 5){
    $var.=" \ <br/>";
  }elseif($failed_attempts == 6){
    $var.="/ \ <br/>";
  }
  $var.="<br/>";  
  
  return $var;
}

?>
<html>
<head>
  <script type="text/javascript" >
    function guess(){
      
    }
  </script>
</head>
<body>
<table>
  <tr>
    <td id="status"><?php print create_hangman(0  );  ?></td>
    <td><input type="text" /></td>
  </tr>
  <tr>
    <?php create_guesser(); ?>
  </tr>
</table>
</body>
</html>