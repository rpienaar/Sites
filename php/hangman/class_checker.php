<?php // CHECKS dependancies in classes

	function check_dependancy($depndant){
		$valid = false;
		foreach(get_included_files() as $inc){
			$inc_tokens = explode("/",$inc);
			if(in_array($depndant,$inc_tokens)){
				$valid = true;
			}
		}
		if(!$valid){
			die("dependancy ".$depndant." module not loaded."); // DEBUG
			//die("") // safer option
		}
		return true;
	}
	
?>