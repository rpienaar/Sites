<html>
	<head>
		<script src="mootools-1.2.4-core-nc.js"></script>
	</head>
	<body>
		<table align="center">
			<tr>
				<td>
				
					<table id="tbl" style="position:absolute;">
						<tr>
							<td>8</td>
							<td>&nbsp;</td>
							<td><input type="radio" id="hr_d2_4" /></td>
							<td>&nbsp;</td>
							<td><input type="radio" id="min_d2_4" /></td>
							<td>&nbsp;</td>
							<td><input type="radio" id="sec_d2_4" /></td>
						</tr>
						<tr>
							<td>4</td>
							<td>&nbsp;</td>
							<td><input type="radio" id="hr_d2_3" style="color:#00f" /></td>
							<td><input type="radio" id="min_d1_3" /></td>
							<td><input type="radio" id="min_d2_3" /></td>
							<td><input type="radio" id="sec_d1_3" /></td>
							<td><input type="radio" id="sec_d2_3" /></td>
						</tr>
						<tr>
							<td>2</td>
							<td><input type="radio" id="hr_d1_2" /></td>
							<td><input type="radio" id="hr_d2_2" /></td>
							<td><input type="radio" id="min_d1_2" /></td>
							<td><input type="radio" id="min_d2_2" /></td>
							<td><input type="radio" id="sec_d1_2"/></td>
							<td><input type="radio" id="sec_d2_2" /></td>
						</tr>
						<tr>
							<td>1</td>
							<td><input type="radio" id="hr_d1_1" /></td>
							<td><input type="radio" id="hr_d2_1" /></td>
							<td><input type="radio" id="min_d1_1" /></td>
							<td><input type="radio" id="min_d2_1" /></td>
							<td><input type="radio" id="sec_d1_1" /></td>
							<td><input type="radio" id="sec_d2_1" /></td>
						</tr>
						<tr>
							<td align="center" >&nbsp;</td>
							<td align="center" >H</td>
							<td align="center" >H</td>
							<td align="center" >M</td>
							<td align="center" >M</td>
							<td align="center" >S</td>
							<td align="center" >S</td>
						</tr>
					</table>
					<table id="table_2" width="150px;" height="120px;" style="position:absolute; left:inherit;" ><tr><td></td></tr></table>
					
				</td>
			</tr>
		</table>
		
		<script type="text/javascript">
			function pop_tbl(){
				$('table_2').Width = $('tbl').Width;
				$('table_2').Height = $('tbl').Height;
			}
			pop_tbl();
		</script>
		
		<script type="text/javascript">
			function ConvertToBinary(dec){
	      var bits = [];
	      var dividend = dec;
	      var remainder = 0;
	      while (dividend >= 2) {
	          remainder = dividend % 2;
	          bits.push(remainder);
	          dividend = (dividend - remainder) / 2;
	      }
	      bits.push(dividend);
	      bits.reverse();
	      return bits.join("");
	    }

			
			// Seconds
			function clock_sec(){
				var d = new Date();
				var curr_sec  = d.getSeconds();
				var str_sec  = pad_lower_than_ten(curr_sec);
				var bin_sec_d1  = pad_8_bit ( ConvertToBinary(parseInt(str_sec.substring(0,1))) );
				var bin_sec_d2  = pad_16_bit( ConvertToBinary(parseInt(str_sec.substring(1,2))) );				
				check_active_bit('sec_d1_1',bin_sec_d1.substring(2,3));
				check_active_bit('sec_d1_2',bin_sec_d1.substring(1,2));
				check_active_bit('sec_d1_3',bin_sec_d1.substring(0,1));
				check_active_bit('sec_d2_1',bin_sec_d2.substring(3,4));
				check_active_bit('sec_d2_2',bin_sec_d2.substring(2,3));
				check_active_bit('sec_d2_3',bin_sec_d2.substring(1,2));
				check_active_bit('sec_d2_4',bin_sec_d2.substring(0,1));
				setTimeout('clock_sec()',298);
			}
			clock_sec();
			
			// Minutes
			function clock_min(){
				var d = new Date();
				var curr_min  = d.getMinutes();
				var str_min  = pad_lower_than_ten(curr_min);
				var bin_min_d1  = pad_8_bit ( ConvertToBinary(parseInt(str_min.substring(0,1))) );
				var bin_min_d2  = pad_16_bit( ConvertToBinary(parseInt(str_min.substring(1,2))) );
				check_active_bit('min_d1_1',bin_min_d1.substring(2,3));
				check_active_bit('min_d1_2',bin_min_d1.substring(1,2));
				check_active_bit('min_d1_3',bin_min_d1.substring(0,1));
				check_active_bit('min_d2_1',bin_min_d2.substring(3,4));
				check_active_bit('min_d2_2',bin_min_d2.substring(2,3));
				check_active_bit('min_d2_3',bin_min_d2.substring(1,2));
				check_active_bit('min_d2_4',bin_min_d2.substring(0,1));
				setTimeout('clock_min()',59999);
			}
			clock_min();
			
			// Hours
			function clock_hour(){
				var d = new Date();
				var curr_hour = d.getHours();
				var str_hour = pad_lower_than_ten(curr_hour);
				var bin_hr_d1 = pad_4_bit ( ConvertToBinary(parseInt(str_hour.substring(0,1))) );
				var bin_hr_d2 = pad_16_bit( ConvertToBinary(parseInt(str_hour.substring(1,2))) );				
				check_active_bit('hr_d1_1',bin_hr_d1.substring(1,2));
				check_active_bit('hr_d1_2',bin_hr_d1.substring(0,1));
				check_active_bit('hr_d2_1',bin_hr_d2.substring(3,4));
				check_active_bit('hr_d2_2',bin_hr_d2.substring(2,3));
				check_active_bit('hr_d2_3',bin_hr_d2.substring(1,2));
				check_active_bit('hr_d2_4',bin_hr_d2.substring(0,1));
				setTimeout('clock_hour()',3599999);		
			}
			clock_hour();
			
			function pad_lower_than_ten(str){
				var s = str+'';
				switch(s.length){
					case 1:
						return '0'+str;
						break;
					case 2:
						return ''+str;
						break;
				}
			}
			
			function check_active_bit(elem,bit){
				switch(bit){
					case '1':
						$(elem).set('checked',true);
						break;
					case '0':
						$(elem).set('checked',false);
						break;
				}
			}
			

			function pad_4_bit(bin){
				var ret = '';
				switch(bin.length){
					case 1:
						ret = '0'+bin;
						break;
					case 2:
						ret = ''+bin;
						break;
					default:
						ret = bin;
						break;
				}
				return ret;
			}
			
			function pad_8_bit(bin){
				var ret = '';
				switch(bin.length){
					case 1:
						ret = '00'+bin;
						break;
					case 2:
						ret = '0'+bin;
						break;
					case 3:
						ret = ''+bin;
						break;
					default:
						ret = bin;
						break;
				}
				return ret;
			}

			function pad_16_bit(bin){
				var ret = '';
				switch(bin.length){
					case 1:
						ret = '000'+bin;
						break;
					case 2:
						ret = '00'+bin;
						break;
					case 3:
						ret = '0'+bin;
						break;
					case 4:
						ret = ''+bin;
						break;
					default:
						ret = bin;
						break;
				}
				return ret;
			}

		</script>
		
	</body>
</html>




















