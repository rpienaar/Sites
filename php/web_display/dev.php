<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
  <meta http-equiv="Pragma" content="no cache" />
  <title>Test HTML</title>
  <script type="text/javascript" src="m.js"></script>
</head>
<body>
  <textarea id="html_preview" name="html_preview" ></textarea>
  <br />
  <button id="switcher" type="button" onclick="make_big();"> >> </button>
  <button onclick="display_the_html();">Display</button>
  <fieldset id="display_preview_html">
    <legend>Html  </legend>
  </fieldset>
  <script type="text/javascript">
    function resize_display_html_block(){
      $('display_preview_html').style.height = (this.getWindow().getSize().y - 175)+"px";
      $('display_preview_html').style.width = (this.getWindow().getSize().x - 50)+"px";
      $('html_preview').style.height = "100px";
      $('html_preview').style.width = "1024px";
    }
    function make_big(){
      $('switcher').set('html',"<<<");
      $('switcher').set('onclick', 'make_small()');
      $('html_preview').style.height = "600px";
      $('display_preview_html').style.height = (this.getWindow().getSize().y - 768)+"px";
    }
    function make_small(){
      $('switcher').set('html',">>");
      $('switcher').set('onclick', 'make_big()');
      $('html_preview').style.height = "100px";
      $('display_preview_html').style.height = (this.getWindow().getSize().y - 175)+"px";
    }
    function display_the_html(){
      $('display_preview_html').set('html',"<legend>Display Html</legend>"+$('html_preview').value);
    }
    window.addEvent('domready',function(){
      resize_display_html_block();
    });
  </script>
</body>
</html>