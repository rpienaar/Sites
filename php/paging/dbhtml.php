<?php
check_dependancy('render.php');
check_dependancy('dbif.php');
check_dependancy('cdbcon.php');
define('BUTTON_MAX',10);

class dbhtml {

	static $dbif;

	function dbhtml(){
		self::$dbif = new dbif();
	}
	
	function entries_html($argument_array){	
		$html = '';
		$table_attributes = $argument_array["table_attributes"];    // html table attributes
		$action_array     = $argument_array["action_array"];        // all the available actions
		$table_limit      = $argument_array["table_limit"];         // limit per table
		$sql_table        = $argument_array["sql_table"];           // sql table/s quering from
		//$table_page       = $argument_array["table_page"];          // the current page where the table will be displayed
		$table_offset     = $argument_array["table_offset"];        // records starting from offset value
		$button_offset    = $argument_array["button_offset"];       // the offset used to know where the buttons will start
		
		$column_array = self::$dbif->table_columns($sql_table);
		$entries_array = self::$dbif->listall_offset($sql_table,$table_offset,$table_limit);
		$table_entries_count = self::$dbif->table_count($sql_table);
		$colspan = count($column_array) + count($action_array);
		
		foreach($column_array as $column){
			$html .= render::ce("td","",render::ce("b","",$column));
		}
		$html .= $this->action_columns($html,$action_array);
		$html = render::ce("tr","",$html);

		if(is_array($entries_array)){ // Rows / Entries
			foreach($entries_array as $entry){	
				$column_entry_html = "";
				foreach($column_array as $column){
					$column_entry_html .= render::ce("td","",$entry->$column);
				}
				$column_entry_html .= $this->action_buttons($column_entry_html,$action_array);
				$html .= render::ce("tr","",$column_entry_html);
			}
		}
		
		if($entries_array != $table_limit){ // the selected button's results already indicate that their are no more records.
			$btn_amount=floor($table_entries_count / $table_limit);
		}else{
			$btn_amount=($table_entries_count / $table_limit);
		}
		
		$prev_html = "";
		if($button_offset + constant('BUTTON_MAX') > constant('BUTTON_MAX')){
			$next_offset = $button_offset * constant('BUTTON_MAX');
			$prev_html = render::ce("button",'onclick="page( '.$next_offset.','.($button_offset - constant('BUTTON_MAX')).' )" ',"Prev");	
		}
		$buttons = render::ce("button",'onclick="page( 0,0 )"',0);
		for($b = $button_offset; $b < ($btn_amount+$button_offset); $b++){
			$break_point = 0;
			if($button_offset == 0){
				$break_point = constant('BUTTON_MAX');
			}else{
				$break_point = $button_offset +constant('BUTTON_MAX');
			}
			if( $b == $break_point) break;
			if( $b == $btn_amount ) break;
			$buttons .= render::ce("button",'onclick="page( '.($table_limit*($b+1)).','.$button_offset.' )" ',($b+1));
		}
		$next_html = "";
		if($btn_amount > constant('BUTTON_MAX') && ($button_offset + constant('BUTTON_MAX')) < $btn_amount){
			$next_offset = $button_offset * constant('BUTTON_MAX');
			$next_html = render::ce("button",'onclick="page( '.($table_limit*($b+1)).','.($button_offset + constant('BUTTON_MAX')).' )" ',"Next");
		}
		$html .= render::ce("tr","",render::ce("td","colspan='".$colspan."'|align='center'",$prev_html.$buttons.$next_html));
		return render::ce("table",$table_attributes,$html);
	}
	
	// columns array, value array, AND $GET, take all the values not mentioned in columns array and value array, and put them into the final URL555
	function construct_get_url(){
		
	}
	
	function action_columns($html,$action_array){
		$html = '';
		foreach($action_array as $act){
			switch($act){
				case "edit":
					$html .= render::ce("td","",render::ce("b","","Edit"));
					break;
				case "delete":
					$html .= render::ce("td","",render::ce("b","","Delete"));
					break;
				default:
					break;
			}
		}
		return $html;
	}
	
	
	function action_buttons($html,$action_array){
		$html = '';
		foreach($action_array as $act){
			switch($act){
				case "edit":
					$html .= render::ce("td","",render::cse("img","src='../img/edit.png'"));
					break;
				case "delete":
					$html .= render::ce("td","",render::cse("img","src='../img/del.gif'"));
					break;
				default:
					break;
			}
		}
		return $html;
	}
	
	function all_dbs_html(){
		$databases = dbif::listall_databases();
		$database_html = '';
		foreach($databases as $d){
			if($d->Database != ''){
			$database_html .= render::ce("option","",$d->Database);
			}
		}
		return $database_html;
	}
	
}

?>