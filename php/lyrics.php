<?php



?>

<html>
	<head>
		<script type="text/javascript" src="mootools-1.2.4-core-nc.js" ></script>
		<script type="text/javascript" >
			function lyrics(){
				var artist = $('artist').value;
				var title = $('title').value;
				var a = new Request({
					url:'lyric_request.php?artist='+artist+"&title="+title,
					method:'get',
					onComplete:function(response){
						$('lyrics').set('html',response);
					}
				}).send();
			}
		</script>
	</head>
	<body>
		Search Lyrics for :<br />
		Artist : <input type="text" id="artist" />
		Song   : <input type="text" id="title" />
		<button onClick="Javascript:lyrics();" >Search</button>
		<textarea id='lyrics' ></textarea>
	</body>
</html>