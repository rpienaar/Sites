<?php

class Page extends CI_Controller {

  public function __construct(){
    parent::__construct();
    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
  }

  public function view($page='index'){

    if ( ! file_exists('application/views/'.$page.'.php') ){
      show_404();
    }

    $data['title'] = 'Title';
    $this->load->view("template/header",$data);
    $this->load->view($page,$data);
    $this->load->view("template/footer",$data);
  }

}