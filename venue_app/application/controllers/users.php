<?php

class Users extends CI_Controller {

  public function __construct(){
    parent::__construct();
    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
    $this->load->model('user_model');
  }

  public function all(){
    $data['title'] = 'User';
    $this->load->view('template/header',$data);
    $this->load->view('users/index',$data);
    $this->load->view('template/footer',$data);
  }

  public function create(){
    $data['title'] = 'User';
    $this->load->view('template/header',$data);

    $this->form_validation->set_rules('email','Email','required');
    $show_create_form = true;
    if( $this->form_validation->run() !== FALSE ){
      if($this->user_model->create()){
        $show_create_form = false;
      }else{
        $show_create_form = true;
      }      
    }
    if($show_create_form){
      $this->load->view('users/create',$data);
    }else{
      $this->load->view('users/success',$data);
    }
    

    $this->load->view('template/footer',$data);
  }

}