<!doctype html>
<html lang="us">
<head>
  <title><?php print $title; ?></title>
  <meta charset="utf-8">

  <link href="<?php echo base_url("res/jquery-ui-1.9.1.custom/css/smoothness/jquery-ui-1.9.1.custom.css"); ?>" rel="stylesheet">

  <script type="text/javascript" src="<?php echo base_url("res/jquery-ui-1.9.1.custom/js/jquery-1.8.2.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("res/jquery-ui-1.9.1.custom/js/jquery-ui-1.9.1.custom.js"); ?>"></script>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url("res/superfish/css/superfish.css"); ?>" media="screen">

  <script type="text/javascript" src="<?php echo base_url("res/superfish/js/hoverIntent.js"); ?>" ?></script>
  <script type="text/javascript" src="<?php echo base_url("res/superfish/js/superfish.js"); ?>" ?></script>

  <script type="text/javascript" >

    // initialise plugins
		jQuery(function(){
			jQuery('ul.sf-menu').superfish();
		});

    $(function() {
        $( "#from_date_time" ).datepicker();
        $( "#to_date_time" ).datepicker();
    });

  </script>

  <script type="text/javascript" src="<?php echo base_url("res/js/main.js"); ?>" ></script>

</head>
<body>

<ul>



</ul>