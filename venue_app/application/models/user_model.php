<?php

class User_model extends CI_Model {

  public function listall(){
    $this->db->get('users');
  }

  public function open_by_email($email){
    $query = $this->db->get_where('user',array('email'=>$email));
    return $query->result_array();
  }

  public function create(){
    if(count( $this->open_by_email($this->input->post('email')) ) > 0){
      return false;
    }else{
      $data = array(
        'group_id' => 1,
        'name' => 'text',
        'password' => md5('password'),
        'email' => $this->input->post('email')
      );
      $this->db->insert('user',$data);
      return true;
    }
  }

}